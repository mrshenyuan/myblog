---
title: Linux Shell 遍历文件夹下php文件并重命名文件名首字母大写
date: 2017-07-25 21:10:03
tags: shell
description: 在Linux下遍历文件夹下的文件并并重命名文件名首字母大写
---

因有一个项目是基于Codeigniter 2.x之前的版本搭建的,控制器和模型的php文件名都是小写，尝试将Codeigniter框架的升为3.1.x版本，手册中控制器和模型下的php文件名必须是大写字母开头。我将旧项目的控制器和模型下的文件，复制到新项目(Codeigniter 3.1.x版本的项目)对应文件夹下，之前手动修改控制器和模型下的文件的名称的首字母都改为大写。

尝试使用 Linux shell 自动遍历文件夹下的文件，并自动修改php文件名的首字母大写。

如：新建一个`rename_controllers.sh`放在新项目的根目录下，内容是；

```
cd application/controllers/ #进入 `application/controllers/` 文件夹
for file in *.php #遍历新项目中`controllers`文件夹下的所有以"php"结尾的文件
do
    if test -f $file
    then
        echo $file
        str=`echo $file|awk '{print substr($0,1,1)}'` #截取php文件的文件名首字母
        mv "$file" `echo $file | sed "s/^./\U$str/"` #重命名当前遍历到的php文件 为 文件名首字母大写的文件名
    fi
done
```

### 参考 ###

1. [shell编程--遍历目录下的文件](http://www.cnblogs.com/kaituorensheng/archive/2012/12/19/2825376.html) 遍歷文件夾
2. [【转】linux下awk内置函数的使用(split/substr/length)](http://www.cnblogs.com/sunada2005/p/3493941.html) 截取首字母
3. [Linux 批量修改文件名]( http://blog.sina.com.cn/s/blog_63fd1bdb01013lv6.html) 替換文件名



