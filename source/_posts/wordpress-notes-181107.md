---
title: '我自己的Wordpress 笔记收集'
date: 2018-11-07 21:06:23
tags: 
  - cart
  - woocommerce
categories:
  - Wordpress
description: '我自己的Wordpress 笔记收集'
---

*  [How to Limit Authors to their Own Posts in WordPress Admin](https://www.wpbeginner.com/plugins/how-to-limit-authors-to-their-own-posts-in-wordpress-admin/) 限制当前用户看到其它作者的文章
* [WooCommerce Cart](https://wordpress.org/plugins/side-cart-woocommerce/) 学习到可以在js中添加 觸發 event和監聽event ，ajax add to cart plugin
* 加入購物車之後的更新操作

1. [woocommerce Class WC_Cart](https://docs.woocommerce.com/wc-apidocs/class-WC_Cart.html)
2. [Get the number of items in cart in wordpress using woocommerce](https://stackoverflow.com/a/42869165)  獲取woocommerce 購物車商品的數量
3. [WHY IS WOOCOMMERCE SO SLOW – HERE IS HOW TO FIX IT!](https://wpfixit.com/why-is-woocommerce-so-slow/) 解決網站慢的問題呀
4. [woocommerce - Get Cart Total as number](https://stackoverflow.com/q/30063173) 列出了 `WC()->cart`的一些方法的結果

* [WooCommerce: Check if Product ID is in the Cart](https://businessbloomer.com/woocommerce-easily-check-product-id-cart/)

* [WooCommerce Class WC_Cart](https://docs.woocommerce.com/wc-apidocs/class-WC_Cart.html)

* [WordPress: How to keep the line breaks in meta box text area field](https://iftakharhasan.wordpress.com/2015/09/23/wordpress-how-to-keep-the-line-breaks-in-meta-box-text-area-field/)

* [WP frontend output of custom textarea fields not respecting line breaks. In admin it's OK](https://wordpress.stackexchange.com/a/112259)

* [Jquery实现页面上所有的checkbox只能选中一个](https://www.cnblogs.com/tomz/p/3425566.html)

* [Make the WooCommerce My Account Navigation Look Awesome with CSS](https://wordx.press/make-great-looking-woocommerce-my-account-navigation-with-css/) 美好 woocommerce 我的账户页面

* [WooCommerce: Automatically Update Cart on Quantity Change](https://businessbloomer.com/woocommerce-automatically-update-cart-quantity-change/) ,[Auto update cart after quantity change](https://wordpress.stackexchange.com/a/304327) woocommmerce 购物车中当数量改变时，自动更新购物车

------

* woocommerce mycred 日志在 woocommerce中分页解决

因为 wordpress 的 `Permalink Settings` 设定为 `Post name`

查考 [WooCommerce Wallet – credit, cashback, refund system](https://wordpress.org/plugins/woo-wallet/) 插件，参考这个插件的结构，在woocommerce中添加了 `endpoint`，主要的功能是显示 mycred 的 会员积分余额和会员积分日志，最大的问题是 `mycard_history` 这个短代码中，分页的链接显示的 `/my-account/[endpoint]/?page=3`,点击这个链接，会跳回 `/my-account/3`页面，分页就处理不到。

当查看了 `http://codex.mycred.me/classes/mycred_query_log/` 中获取页码有

```
/**
 * Get Page Number
 * @since 1.4
 * @version 1.0.3
 */
public function get_pagenum() {

    global $wp;

    $page_key = ( isset( $this->args['page_arg'] ) && $this->args['page_arg'] !== NULL && $this->args['page_arg'] != '' ) ? $this->args['page_arg'] : 'page';
    if ( isset( $wp->query_vars[ $page_key ] ) && $wp->query_vars[ $page_key ] != '' )
        $pagenum = absint( $wp->query_vars[ $page_key ] );

    elseif ( isset( $_REQUEST[ $page_key ] ) )
        $pagenum = absint( $_REQUEST[ $page_key ] );

    elseif ( isset( $_REQUEST[ $page_key ] ) )
        $pagenum = absint( $_REQUEST[ $page_key ] );

    else return 1;

    return max( 1, $pagenum );

}
```

`$this->args['page_arg']` 可以通过 `http://codex.mycred.me/filters/mycred_query_log_args/` 的 `mycred_query_log_args` filter 覆盖 `page_arg`的参数名称，改为 定义的 `[endpoint]` 名称之后，分页就可以了

如:
```
function custom_mycred_transactions_args($args){
    $args['page_arg'] = '[endpoint]';
    return $args;
}
add_filter('mycred_query_log_args','custom_mycred_transactions_args'));
```

------

* [WordPress, WP Parser and Good Habits](http://jason-boyle.com/wordpress-wp-parser-good-habits/)然後依次創建3個頁面，3個頁面選擇該主題提供的模板即可哦

------

*  wordpress 打开媒体库弹窗 


##### Reference #####
1. [How to include the WordPress media selector in your plugin](https://jeroensormani.com/how-to-include-the-wordpress-media-selector-in-your-plugin/) 弹出媒体库和选择一个文件
2. [Show only images and videos in a wp.media window](https://wordpress.stackexchange.com/a/268597) 只显示指定类型的文件
3. [ericandrewlewis/wp-media-javascript-guide](https://github.com/ericandrewlewis/wp-media-javascript-guide) 文档
4. [How to Create a Custom WordPress Post Type With Image Uploads](https://www.skyverge.com/blog/custom-post-type-with-image-uploads/)

------

* woocommerce 注册默认角色 

##### Reference #####
1. [Assign another role instead of Customer used by WooCommerce](https://www.role-editor.com/woocommerce-assign-role-instead-customer/)