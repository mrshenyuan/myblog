---
title: 'Linxu執行 rm -rf Argument list too long'
date: 2019-06-26  21:58:35
tags:
  - rm
categories:
  - Linux
description:  'Linxu執行 rm -rf Argument list too long'
---

因爲docker容器中跑的Jenkins 有1個任務是名称叫`Jenkins Update`总是失败，而且定时是`* * * * *`每分钟执行的，导致文件夹`jobs`文件夹过多

参考文章的第1篇中，了解到，可以使用`awk`工具进行处理

进入需要删除的文件夹中,然后删除里面的`文件`

```
cd /home/vagrant/test
ls -l| awk '{ print "rm -f ",$9}'|sh
```

![](https://raw.githubusercontent.com/wakasann/diarynote/master/draft/images/190626/20190626145856.png)

因为Jenkins的`builds`下是数字名称的文件夹，所以命令需要修改一下

```
cd ~/docker/jenkins/jobs/Jenkins Update/builds
ls -l| awk '$9<=120254{ print "sudo rm -rf ",$9}'|sh
```
![](https://raw.githubusercontent.com/wakasann/diarynote/master/draft/images/190626/20190626150656.png)

`$9` 是 `ls -l` 第9列，因为我的文件夹是 以数字命名的名称，所以加了数字的判断 

##### Reference
1. [Linux下通过 rm -f 删除大量文件时报错：Argument list too long](https://www.cnblogs.com/-abm/p/9283896.html)
2. [awk从放弃到入门（1）：awk基础 （通俗易懂，快进来看](https://blog.csdn.net/liang5603/article/details/80855386)
3. [一. AWK入门指南](https://awk.readthedocs.io/en/latest/chapter-one.html)
4. [awk实战：文件里面全是数字，实现大于100的求和并打印出所在行和这些数字的总和](https://blog.51cto.com/chaorenhuifei/1704339)