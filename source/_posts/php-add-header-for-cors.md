---
title: PHP 添加 跨域头
date: 2019-07-28 14:12:49
tags: 
  - php
  - cors
categories:
  - php
description: WPHP 添加 跨域头
---

我将下面的代码，放在`Codeigniter` 项目中的`index.php` 中的

```
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Accept, Access-Control-Request-Method, Referer, Content-Type, Content-Length, Accept-Encoding');
//运行请求的方法
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
//当遇到 OPTIONS 请求时，不执行，直接接收
if ( "OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
    die();
}
```

`Access-Control-Allow-Origin`可以是特定的地址,如 .http://localhost:8001 只有 http://localhost:8001 能够访问




##### References
1. [php – 在codeigniter中启用cors(restserver by @chriskacerguis)](https://codeday.me/bug/20190324/820754.html)
2. [[已解决] 急求！如何允许跨域访问？](https://codeigniter.org.cn/forums/thread-23955-1-1.html) 如果所有请求都需要这个请求头设定，直接把这些代码放入到  `index.php` 中 :smile:
3. [Enable CORS origin resource sharing on codeigniter REST SERVER](https://www.competa.com/blog/enable-cors-origin-resource-sharing-on-codeigniter-rest-server/) 文章中有介绍 什么是 CROS，看来自己以后写博文的时候，可能也要加一些对出现错误的专业词语的介绍
4. [enabling cors](https://github.com/chriskacerguis/codeigniter-restserver/issues/345#issuecomment-55420571) codeigniter-resetserver 中的 `application/config/rest.php` 中的`allowed_cors_headers`和`allowed_cors_methods` 是可以参考的