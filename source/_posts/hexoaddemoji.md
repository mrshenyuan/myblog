---
title: Hexo中添加emoji表情
date: 2016-10-03 20:35:16
tags: emoji
description: Hexo中添加emjo表情
---

国庆的三天假前，都是玩CF和LOL的无限乱斗过来的，输了怨没随机到好的英雄，赢了就高高兴兴的:smile: .

在假期的最后一天，感觉时间过的太快，靠吃饭的技能没提升，虚度的时光倒是溜走了。

看了参考文献之后，原来将markdown 变成html的转换器叫做`markdown渲染器`.在Hexo中默认的markdown渲染器 使用的是[hexo-renderer-marked](https://github.com/hexojs/hexo-renderer-marked),是Hexo版本，这个渲染器不支持插件扩展。另外一个 markdown 渲染器 [hexo-renderer-markdown-it](https://github.com/celsomiranda/hexo-renderer-markdown-it)，这个支持插件配置，可以使用 [markwon-it-emoji](https://github.com/markdown-it/markdown-it-emoji)插件来支持emoji。需要将原来的 `marked` 渲染器换成 `markdown-it`渲染器。我使用的Hexo3

## 安装过程 ##

### 安装新的渲染器 ###
首先进入博客目录,卸载hexo默认的`marked`渲染器，安装`markdown-it`渲染器，运行的命令如：
```shell
cd Documents/blog
npm un hexo-renderer-marked --save
npm i hexo-renderer-markdown-it --save
```
之后安装`markdown-it-emoji`插件：
```shell
npm install markdown-it-emoji --save
```
### 编辑站点配置文件 ###
这里的站点配置文件是指位于博客根目录下的 `_config.yml`，编辑它，然后在末尾添加如下内容：
```yml
# Markdown-it config
## Docs: https://github.com/celsomiranda/hexo-renderer-markdown-it/wiki
markdown:
  render:
    html: true
    xhtmlOut: false
    breaks: true
    linkify: true
    typographer: true
    quotes: '“”‘’'
  plugins:
    - markdown-it-abbr
    - markdown-it-footnote
    - markdown-it-ins
    - markdown-it-sub
    - markdown-it-sup
    - markdown-it-emoji  # add emoji
  anchors:
    level: 2
    collisionSuffix: 'v'
    permalink: true
    permalinkClass: header-anchor
    permalinkSymbol: ¶
```
上面的是`hexo-renderer-markdown-it`的所有选项的配置，详细的每一项配置说明，需要到[Advanced Configuration](https://github.com/celsomiranda/hexo-renderer-markdown-it/wiki/Advanced-Configuration)中查看。

### 给新的渲染器添加twemoji ###
因为安装了`markdown-it-emoji`, `:smile:`渲染成😄了，是Unicode字符表情。感觉不好看，参考文章的介绍，安装[twemoji](https://github.com/twitter/twemoji),安装命令如下:
```shell
npm install twemoji
```
安装完之后，编辑`node_modules/markdown-it-emoji/index.js`文件，最终文件像:
```js
'use strict';


var emojies_defs      = require('./lib/data/full.json');
var emojies_shortcuts = require('./lib/data/shortcuts');
var emoji_html        = require('./lib/render');
var emoji_replace     = require('./lib/replace');
var normalize_opts    = require('./lib/normalize_opts');
var twemoji           = require('twemoji')  //添加twemoji


module.exports = function emoji_plugin(md, options) {
  var defaults = {
    defs: emojies_defs,
    shortcuts: emojies_shortcuts,
    enabled: []
  };

  var opts = normalize_opts(md.utils.assign({}, defaults, options || {}));

  md.renderer.rules.emoji = emoji_html;
  //使用 twemoji 渲染
  md.renderer.rules.emoji = function(token, idx) {
	return twemoji.parse(token[idx].content);
  };

  md.core.ruler.push('emoji', emoji_replace(md, opts.defs, opts.shortcuts, opts.scanRE, opts.replaceRE));

 
};

```
因为我安装twemoji的2.2.0之后，好像默认的是`72X72`,假如你不喜欢这个图片尺寸，可以通过css控制，在你的主题css中添加如:
```css
img.emoji {
   height: 1em;
   width: 1em;
   margin: 0 .05em 0 .1em;
   vertical-align: -0.1em;
}
```
的css代码，代码来自[Tips](https://github.com/twitter/twemoji#tips),，是控制同行显示的。

---
由于我个人使用的是[hexo-theme-next](https://github.com/iissnan/hexo-theme-next)主题，导致最终渲染的文章中的emoji图片，会自动绑定`fancybox`,而且有边框和图片看起来很小，需要修改两个文件。
1. 编辑 `themes/next/source/js/src/utils.js`
将文件中的`$('.content img').not('.group-picture img')`替换为`$('.content img').not('.group-picture img,.emoji')`,这是为了防止生成的emoji图片被`fancybox`绑定，点击表情的图片，放大表情。
2. 在`themes/next/source/css/_custom/custom.styl`中添加

```css
img.emoji {
   height: 1.5em;
   width: 1.5em;
   margin: 0 .05em 0 .1em !important;
   vertical-align: -0.1em;
   //override theme default style
   padding:0px !important;
   border:none !important;
   display:inline !important;
}
```

---

重新启动Hexo本地站点就可以看到 Unicode字符表情变成了图片表情。如`:blush: ` :blush: 

### 总结 ###
明白markdown装html的叫做渲染器，学到npm卸载的命令。尝试了[emoji-cheat-sheet.com](https://github.com/WebpageFX/emoji-cheat-sheet.com/) 中的`emoji-parser`,暂时换不到，目前就用`twemoji`也不错，以后写博客的时候，加点表情或许更有意思呢。Hexo 开启调试模式的命令是`hexo s --debug`:v:

### 参考文献 ###
1. [让Hexo支持emoji表情](http://www.sumyblog.me/2016/02/29/hexo-support-emoji/)
2. [Hexo添加emoji](http://very9s.net/post/hexo-support-emoji/)
