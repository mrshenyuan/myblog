---
title: 'apache保护Laravel .env 不能直接被访问'
date: 2019-04-10 22:39:47
tags: 
categories:
  - 安全
description: 'apache 保护Laravel .env 不能直接被访问'
---

如果部署的Laravel项目，web 虚拟主目录不是指向 laravel项目中的 public, 就可以通过浏览器访问到 非 public目录下的文件，如 保护 .env 不被访问到

如web 服务器是用 apache,可以添加一个 .htaccess 在 项目根目录，内容如下:

```
# Disable index view
Options -Indexes

# Hide a specific file
<Files .env>
    Order allow,deny
    Deny from all
</Files>

```