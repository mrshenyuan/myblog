---
title: '尝试jira 和 confluence 整合'
date: 2019-08-11 21:05:43
tags: 
  - confluence
  - jira
categories:
  - 零碎笔记
description: '尝试jira 和 confluence 整合'
---

因为我先安装了 confluence,再安装 jira的，未能没找到整合的方法,atlassian官网的文档自己没耐心看，只好重新安装 confluence. 
先安装jira,再安装 confluence.

参考:
1. [JIRA与confluence的用户整合](https://blog.csdn.net/shandong_chu/article/details/78561814)
2. [JIRA 7.8 版本的安装与破解](https://www.cnblogs.com/houchaoying/p/9096118.html)
3. [Centos下安装破解confluence6.3的操作记录](https://www.cnblogs.com/kevingrace/p/7607442.html)