---
title: '保护Laravel .env文件，防止直接访问'
date: 2019-08-11 16:20:57
tags:
  - laravel
  - apache
  - env
categories:
  - 运维
description: '保护Laravel .env文件，防止直接访问'
---

web服务器:  Apache

服务器系统: Ubuntu 14.04

如果不是vhost的形式部署在服务器上，可能是可以通过 `http://www.example.com/.env` 查看到laravel的配置信息，这样不安全。

方法1: 在Laravel项目的根目录下创建一个 `.htaccess` 文件，里面的内容为



```
#Disable index view
options -Indexes

#hide a specific File

<Files .env>
order allow,deny
Deny from all
</Files>
```