---
title: 'Homestead 安装其它的PHP版本'
date: 2019-08-11 16:13:35
tags:
  - php开发环境
  - homestead
categories:
  - Linux
description:  'Homestead 安装其它的PHP版本'
---

运行环境:

系统: win10

软件: 

1. virtualbox 6.2
2. vagrant 2.2.4 
3. homestead 7.1.0 

```
sudo apt-get update
sudo apt-get -y install php5.6-mysql php5.6-fpm php5.6-mbstring php5.6-xml php5.6-curl
```

安装中出现
```
Fetched 3,466 kB in 6min 24s (9,022 B/s)                                                                                       
E: Failed to fetch http://ppa.launchpad.net/ondrej/php/ubuntu/pool/main/p/php5.6/php5.6-common_5.6.40-8+ubuntu18.04.1+deb.sury.org+1_amd64.deb  Connection timed out [IP: 91.189.95.83 80]
E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
```

这是因为下载超时导致的，继续执行

```
sudo apt-get -y install php5.6-mysql php5.6-fpm php5.6-mbstring php5.6-xml php5.6-curl
```

进行重试进行安装 php 5.6