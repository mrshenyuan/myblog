---
title: '15.01升级到 16.04之后，无法启动mysql'
date: 2019-08-11 21:27:03
tags: 
  - mysql
categories:
  - 零碎笔记
description: '15.01升级到 16.04之后，无法启动mysql'
---

参考 [16.04 Distribution Upgrade - cannot start MySQL server](https://ubuntuforums.org/showthread.php?t=2359827&p=13639026#post13639026)，然后找到了[16.04 upgrade broke mysql-server](https://askubuntu.com/a/762432)


我由 15.01升级到 16.04之后，无法启动mysql,以上方法解决了，而且数据库也还在~~



备份你服务器中`/etc/mysql`下的 `my.cnf` 文件  和 删除或者重命名它。

```
sudo mv /etc/mysql/my.cnf /etc/mysql/my.cnf.bak
```
删除不使用的`/etc/mysql/mysql.conf.d/` 文件夹

```
sudo rm -r /etc/mysql/mysql.conf.d/
```
验证你没有在其它地方有隐藏的`my.cnf` 文件

```
sudo find / -name my.cnf
```

备份和移除`/etc/mysql/debian.cnf`的文件(不确认是否是需要的，但万一有关呢)

```
sudo mv /etc/mysql/debian.cnf /etc/mysql/debian.cnf.bak
sudo apt purge mysql-server mysql-server-5.7 mysql-server-core-5.7
sudo apt install mysql-server
```
假如你的系统日志显示的错误像`mysqld: Can't read dir of '/etc/mysql/conf.d/'`，创建1个软链接:

```
sudo ln -s /etc/mysql/mysql.conf.d /etc/mysql/conf.d
```

mysql 服务应该可以通过` sudo service mysql start`来启动了

------

自己好像是试过上面的命令，过了一年之后，再补充，忘记了.....