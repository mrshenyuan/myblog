---
title: 'yii2 vendor/bower/jquery/dist not exist'
date: 2019-07-28 13:59:17
tags: 
  - Yii2
categories:
  - Yii2
description: 'yii2 vendor/bower/jquery/dist not exist'
---

查看 vendor 文件夹，只有`bower-asset`文件夹

手动修改 `bower-asset` 为`bower` 倒也可以，yii2项目每次 composer install 成功之后，每次重命名这个文件夹，有点麻烦

原来是忘记安装[fxp/composer-asset-plugin](https://packagist.org/packages/fxp/composer-asset-plugin) 这个 composer asset plugin 扩展

执行下面的命令，安装这个扩展

```
composer global require "fxp/composer-asset-plugin:^1.4.5"
```

安装完之后需要执行

```
composer install -vvv
```

`-vvv` 是啰嗦模式，可以看到执行的情况