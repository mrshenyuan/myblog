---
title: Ionic POST提交使用普通表单提交数据
date: 2019-07-28 14:12:17
tags: 
  - ionic
  - post
  - formdata
categories:
  - php
description: Ionic POST提交使用普通表单提交数据
---
使用 和 GET 拼接参数一样拼接

```
doLogin() {
   let url = "http://loginApiUrl";

  var headers = new Headers()
   headers.append('Content-Type', 'application/x-www-form-urlencoded');

  let options = new RequestOptions({ headers: headers });


  let body =  "username=" +'test' + "&password=" + 'admin@mac.com';

  console.log("Form Data:"+body);

  return this.http.post(url, body, options) .map(res => res.json()).subscribe(
              data => {
                console.log(data);
              },
              err => {
                console.log("ERROR!: ", err);
              }
          );
}
```

这个参数放入 post 请求的 body 中

##### References
1. [How to send Http Post parameter using x-www-form-urlencoded in Ionic 2
](https://stackoverflow.com/a/49173525)
2. [Form data is empty when calling http.post() with ContentType header](https://github.com/angular/angular/issues/13241#issuecomment-264905597) 循环对象，使用类似键值对的方式放入数组，然后再使用 js的数组 join方法