---
title: '[转发]Lumen 使用事件需要注意的事项'
date: 2019-03-14 22:06:23
tags: 
  - event
  - 事件
categories:
  - Laravel
description: '[转发]Lumen 使用事件需要注意的事项'
---

Lumen

版本 5.2

参考手册 [laravel event](https://learnku.com/docs/laravel/5.4/events/1250)

需要注意的事项

如果是第一次在lumen下使用事件，需要修改`bootstrap\app.php`文件 添加对`EventServiceProvider` 的注册(一般是被注释掉了，取消注释即可)

```
$app->register(App\Providers\EventServiceProvider::class);
```

在编写Listener的时候，注意`use` 需要监听的`Event`类

使用异步事件的时候，需要使用队列，请先配置好队列，需要启动监听，命令如下：

```
php artisan queue:listen --queue:default 
```

##### Reference ######

1. 出处: [Lumen 使用事件需要注意的事项](https://www.cnblogs.com/cshua/p/7084353.html)