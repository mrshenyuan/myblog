---
title: 'Yii2 在php 7.2环境下运行，提示 Cannot use ‘Object’ as class name'
date: 2019-07-28 14:00:12
tags:
  - Yii2
categories:
  - Yii2
description: 'Yii2 在php 7.2环境下运行，提示 Cannot use ‘Object’ as class name'
---

出错原因是:

`Object`是php7.2中的保留类名，不可以使用`Object`作为类的名称。

> The object name was previously soft-reserved in PHP 7.0. This is now hard-reserved, prohibiting it from being used as a class, trait, or interface name.

执行

```
composer update
```

更新 Yii2的版本.

------

从[Object.php](https://github.com/yiisoft/yii2/blob/master/framework/base/Object.php)文件的注释(在本地中的路径是`vendor/yiisoft/yii2/base/Object.php`)，从 Yii2 2.0.13版本开始，已经弃用了 `Object`类，使用`BaseObject` 进行代替

如果安装的使用的类库继承了`yii\base\Object`,可以修改为`yii\base\BaseObject`

如安装了`mdmsoft/yii2-admin`,需要将`vendor/mdmsoft/yii2-admin/components/Configs.php` 中，将`\yii\base\Object` 修改为`\yii\base\BaseObject`


##### References
1. [PHP7.2中Yii2核心类Object报错](https://www.maoxuner.cn/2018/02/04/php7.2-yii2-object-conflict.html)

