---
title: 'Ubuntu install ibus-pinyin'
date: 2019-02-27 21:09:47
tags: 
  - pinyin
categories:
  - Linux
description: 'Ubuntu install ibus-pinyin'
---

```
sudo apt-get install ibus-pinyin # 安装ibus拼音
sudo ibus-setup # 启动ibus的设置页面
```

##### Reference #####

1. [Ubuntu 16.04安装iBus中文输入法pinyin及问题](https://blog.csdn.net/suifenghahahaha/article/details/78723733)