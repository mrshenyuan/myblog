---
title: Chrome DNS_PROBE_FINISHED_NXDOMAIN
date: 2019-07-28 13:16:22
tags:
  - chrome
categories:
- Chrome
description: Chrome DNS_PROBE_FINISHED_NXDOMAIN
---
win10 下的Chrome访问网站时，提示`DNS_PROBE_FINISHED_NXDOMAIN`

![](/uploads/190720/20190720100642.png)

解决方法很简单：

用管理员身份打开cmd后，运行如下指令即可解决问题。

![](/uploads/190720/20190720100843.png)

运行命令:

```
netsh winsock reset
```

具体操作，如下图：

![](/uploads/190720/20190720101126.png)

注明: 在 cmd中，点击右键，可以起到粘贴的左右(如果有复制文本的情况下)

##### References
1. [DNS_PROBE_FINISHED_NXDOMAIN错误的解决方法](https://blog.csdn.net/leekwen/article/details/73530337)