---
title: Vagrant box ubuntu/xenial64 添加vagrant用户解决没有登录密码的问题
date: 2017-06-19 23:39:57
tags: vagrant
description: Vagrant box ubuntu/xenial64 添加vagrant用户解决ubuntu用户名没有登录密码的问题
---

是参考[Vagrant box ubuntu/xenial64 の ubuntuユーザ の passwordについて](http://qiita.com/TKR/items/f27271c963de0033f7ff)进行处理的。

1.可以通过 Git Bash 使用  vagrant ssh 登录到Ubuntu/xenial64的终端中

2.在终端中输入: vim 

.useradd.sh 文件内容

```
#!/bin/bash
set -Ceu

USER="vagrant"
# password "vagrant" を SHA-512 でハッシュ化
PASSWORD=$(perl -e 'print crypt("vagrant", "\$6\$");')

sudo useradd -p ${PASSWORD} -m ${USER}
```
将内容复制的打开的编辑器当中，然后 按 ESC键，输入":w useradd.sh"

3.执行 useradd.sh,`sh ./useradd.sh`,执行完之后，可以尝试 su vagrant 命令，使用用户名:vagrant,密码:vagrant 进行切换用户。

4.切换到vagrant之后， 执行sudo apt-get update,提示"vagrant is not in the sudoers file. This incident will be reported."的消息。

5.在git bash 中输入exit,退出当前vagrant 登录的用户，切换到默认的ubuntu用户下。

下一步需要将vagrant 用户添加到 /etc/sudoers 

1. 添加文件的写权限。输入命令"chmod u+w /etc/sudoers"。
2. 编辑/etc/sudoers文件。输入命令"vim /etc/sudoers",进入编辑模式，找到这一 行："root ALL=(ALL) ALL"在起下面添加"vagrant ALL=(ALL) ALL"，然后保存退出。
3. 撤销文件的写权限。输入命令"chmod u-w /etc/sudoers"。
4. 尝试切换到vagrant 用户，输入命令"su vagrant",输入密码:vagrant ,尝试运行"sudo apt-get update",提示输入密码，再次输入 vagrant密码之后，应该就会执行 sudo apt-get update 命令了。


备注: 添加vagrant 用户为了能用一个软件可以通过ssh方式登录进去，笔者添加用户的目的是 可以通过xshell5软件能够登录到xenial。

### 参考 ###

1. [Vagrant box ubuntu/xenial64 の ubuntuユーザ の passwordについて](http://qiita.com/TKR/items/f27271c963de0033f7ff) 添加vagrant 用户
2. [Ubuntu技巧之 is not in the sudoers file解决方法](http://www.linuxidc.com/Linux/2010-12/30386.htm) 解决vagrant用户不在 sudoers 文件中的提示信息
3. [Vagrant's Ubuntu 16.04 vagrantfile default password](https://stackoverflow.com/a/41337943)