---
title: Docker的镜像 导出导入
date: 2019-07-28 14:25:29
tags:
  - Docker
categories:
  - Docker
description: 'Docker的镜像 导出导入'
---
查看当前已经安装的镜像

```
vagrant@vagrant:~$ sudo docker images
```

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mysql               5.7.22              6bb891430fb6        11 months ago       372MB
```

#### 导出镜像

```
sudo docker save  [IMAGE ID] > [filename].tar
```
如导出上面的列出的mysql 镜像

```
sudo docker save  6bb891430fb6 > mysql5722.tar
```

#### 导入镜像

```
sudo docker load --input [filename].tar
```

如 导入刚才导出的`mysql5722.tar`镜像

```
sudo docker load --input mysql5722.tar
```

导入镜像之后，通过`sudo docker images`，发现导入的镜像的`REPOSITORY`和`TAG`都是`<none>`，使用 `tag`命令，给 导入的镜像设定`REPOSITORY`和`TAG`，

发现同一个 导出镜像的`IMAGE ID`和导入镜像的`IMAGE ID`是一样的，所以可以通过导出之前的`sudo docker images` 的列表，给导入的镜像设置一样的`REPOSITORY`和`TAG`

```
sudo docker tag [IMAGE ID] [REPOSITORY]:[TAG]
```

如: 上面列出的 `mysql` 镜像，导入镜像之后，设`REPOSITORY`和`TAG`

```
sudo docker tag 6bb891430fb6 mysql:5.7.22
```

------

题外话:在国内的网络中，如果无法安装到一个镜像(使用了国内镜像之后或者重试很多次都是超时的情况下)，可以在另外一个能访问到不能安装镜像包的主机先把镜像下好，然后导出，下载到本地，再导入使用 :smile:

##### References
1. [Docker images导出和导入](https://www.jianshu.com/p/8408e06b7273) 讲的很详细
2. [docker images 导入导出](https://blog.csdn.net/u011365831/article/details/81430513) 导入导出的命令，一看就会了 :smile: