---
title: laravel passport client_credentials
date: 2018-10-31 21:43:49
tags:
  - laravel
  - passport
categories:
  - Laravel
description: laravel passport client_credentials
---

我是使用 Laravel 5.4 + Dingo Api + passport/jwt 两个验证方式

目前需要用到 passport 的 `client_credentials`

获取 token成功之后，如果需要验证 token的有效，需要在

1. `app\Http\Kernel.php` 的 `$routeMiddleware` 中 添加一个 

```
//客户端证书发放令牌验证中间件
'client_credentials'     => \App\Http\Middleware\CheckClientCredentials::class,
```

如:

```
protected $routeMiddleware = [
    'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
    'can' => \Illuminate\Auth\Middleware\Authorize::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
    'jwt.refresh' => \Tymon\JWTAuth\Middleware\RefreshToken::class,
    'jwt.generalize.auth' => \App\Http\Middleware\GeneralizeAuth::class,
    'cors' => \Barryvdh\Cors\HandleCors::class,
    //客户端证书发放令牌验证中间件
    'client_credentials'     => \Laravel\Passport\Http\Middleware\CheckClientCredentials::class,
];
```

2. 然后在路由中需要验证的中间件数组中，添加 `client_credentials` 就可以了。

如:

```
<?php
use Illuminate\Http\Request;
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api){
    $api->group([
        "prefix"=>"test",
        'middleware' => [
            'client_credentials', //client_credentials 类型验证的中间件
            'cors'
        ],
        'namespace' => 'App\Api\Test\Controllers',
    ], function ($api) {

        $api->get('test','TestController@test');
    });

});
```

------

Laravel Passport Key path oauth-public.key does not exist or is not readable

You do not mention your installation steps. Presume you did the following:

```
composer require laravel/passport
```

Register the service provider inside `config/app.php`

Laravel\Passport\PassportServiceProvider::class,

Run the migrations

```
php artisan migrate
```

only run passport migrate

```
php artisan migrate --path=vendor/laravel/passport/database/migrations
```

Lastly generate the keys using

```
php artisan passport:install
```


Reference [Laravel Passport Key path oauth-public.key does not exist or is not readable](https://stackoverflow.com/a/39417466/5840474)

-----

#### 参考: 

1. [[ Laravel 5.4 文档 ] 安全 —— API认证（Passport）](https://laravelacademy.org/post/6813.html)
2. [Laravel 的 API 认证系统 Passport](https://laravel-china.org/docs/laravel/5.4/passport/1240#921b32)
3. [Grant Type client_credentials Authentication fails](https://github.com/laravel/passport/issues/101#issuecomment-249202379) 处理好client_credentials的验证问题
4. [API Authentication (Passport)](https://laravel.com/docs/5.7/passport#client-credentials-grant-tokens) 原来官网文档已经有如何验证部分的案例啦 :smile: