---
title: php 连接mysql 主机 localhost，显示 No such file or directory
date: 2019-07-03 22:39:02
tags:
  - mysql
  - php
categories:
  - PHP
description: php 连接mysql 主机 localhost，显示 No such file or directory
---

打开 `php.ini`文件，找到这1行

```
mysql.default_socket
```

然后将它修改为

```
mysql.default_socket=/path/to/mysql.sock
```

`/path/to/mysql.sock`是mysql的配置文件`/etc/my.cnf`中的`socket`的路径

如我当前虚拟机中的mysql 配置文件`/etc/my.cnf`的`socket = /tmp/mysql.sock`,所以修改`php.ini`中找到的`mysql.default_socket = `都改为`mysql.default_socket = /tmp/mysql.sock`，然后重启 apache，使配置生效

-------

系统:Ubuntu 16.04
环境: Apache 2.4 + PHP 5.6.40 + Mysql 5.5.31

php连接mysql demo代码:

```
$conn = mysql_connect('localhost','root','secret');

if(!$conn){
	die(mysql_error());
}

echo "success";	
```

主机使用`localhost`，显示`No such file or directory`的错误。改为`127.0.0.1`之后可以连接成功。

前提: 我的mysql配置文件 `/etc/my.cnf`中`[mysqld]`节点下的配置项 `socket = /tmp/mysql.sock`

终端命令(用来查看 php 信息中`mysql.default_socket`的配置情况):

```
php -i | grep mysql.default_socket
```

运行结果:

```
mysql.default_socket => no value => no value
pdo_mysql.default_socket => /var/run/mysqld/mysqld.sock => /var/run/mysqld/mysqld.sock
```

php.ini中`mysql.default_socket`的默认路径和当前运行mysql 配置文件中的`socket`的值`/tmp/mysql.sock` 不相等

我的LAMP环境中`php.ini`文件有2个，路径分别是:`/etc/php/5.6/cli/php.ini`和`/etc/php/5.6/apache2/php.ini`

将上面2个`php.ini`文件中`mysql.default_socket = ` 都修改成`mysql.default_socket = /tmp/mysql.sock`，然后重启apache2

终端命令:

```
sudo service apache2 stop # 停止apache
sudo service apache2 start # 启动apache
```

再次查看 `mysql.default_socket`的配置是否生效

终端命令:

```
php -i | grep mysql.default_socket
```

运行结果:

```
mysql.default_socket => /tmp/mysql.sock => /tmp/mysql.sock
pdo_mysql.default_socket => /tmp/mysql.sock => /tmp/mysql.sock
```

配置成功，然后通过浏览器，访问带有上面 `php连接mysql demo代码`的文件，运行结果为`success`.


##### Reference 
1. [MySQL connection not working: 2002 No such file or directory](https://stackoverflow.com/a/1676741)