---
title: 'centos php 7.0.31 to 7.2.9'
date: 2019-08-11 16:36:30
tags:
  - lamp
categories:
  - 运维
description: 'centos php 7.0.31 to 7.2.9'
---


安装php7的yum源,如已安裝，可跳過，我之前已经执行过一次了

```
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
```

因为我之前将 

```
vim /etc/yum.repos.d/remi-php70.repo
```
将[remi-php70]段中的enabled=0改为enabled=1，是启用，然后现在要禁用它，相反的，将enabled=1改为enabled=0即可。

然后
```
vim /etc/yum.repos.d/remi-php72.repo
```

将[remi-php72]段中的enabled=0改为enabled=1

```
yum list php
```

yum 安装php7.2

```
yum install php72
```

安装完之后，运行 `php -v`会还是显示 7.0.x,需要运行一次

```
yum update
```

最后执行

```
service httpd restart
```