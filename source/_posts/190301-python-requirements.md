---
title: python 依赖文件和安装依赖
date: 2019-03-01 21:09:47
tags: 
  - 依赖
categories:
  - Python
description: python 依赖文件和安装依赖
---

在 python 项目的目录下

生成requirements.txt文件

```
pip freeze > requirements.txt
```

安装requirements.txt依赖

```
pip install -r requirements.txt
```

##### Reference #####
1. [如何自动生成和安装requirements.txt依赖`](https://www.cnblogs.com/zelos/p/7439599.html)
2. [解决Python开发过程中依赖库打包问题的方法](https://www.cnblogs.com/maoguy/p/6416392.html)