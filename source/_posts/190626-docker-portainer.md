---
title: 'Docker 安装  portainer'
date: 2019-06-26  22:30:35
tags:
  - portainer
categories:
  - Docker
description:  'Docker 安装  portainer'
---

dockerhub上的 [portainer/portainer](https://hub.docker.com/r/portainer/portainer)

1. 拉取镜像

```
sudo docker pull portainer/portainer
```
2. 运行创建portainer

```
sudo docker run -d -p 9000:9000 \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --name ptest portainer/portainer
```

就可以通过浏览器访问 `http://ip:9000` 进入管理页面。初次启动需要选择 `EndPoints`和设置`admin`的密码。
因为我是单机运行的docker,所以`EndPoints`选择的 `Local`

![](https://raw.githubusercontent.com/wakasann/diarynote/master/draft/images/190626/20190626152414.png)



```
sudo docker exec -it containerName /bin/bash #使用exec命令 进入容器中的ssh,containerName是容器名称
sudo docker start containerName #根据容器名称启动一个容器
sudo docker stop containerName #根据容器名称停止一个容器
sudo docker restart containnerName #根据容器名称重启一个容器
```



##### Reference

1. [Docker 部署 portainer](https://www.cnblogs.com/xiangsikai/p/10291643.html)
2. [Docker容器的创建、启动、和停止](https://www.cnblogs.com/linjiqin/p/8608975.html)
3. [Portainer快速使用](https://www.kancloud.cn/websoft9/docker-guide/828253)