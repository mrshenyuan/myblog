---
title: '一些使用Yii2时遇到的问题解决方法记录'
date: 2019-05-11 22:39:47
tags: 
categories:
  - Yii2
description: '一些使用Yii2时遇到的问题解决方法记录'
---

*  yii2 activeform 替換 form-gruop [How to add class to form-group div of ActiveField in YII2?](https://stackoverflow.com/a/34758598)

* yii2 file input 如果需要有移除效果，要配置`initialPreviewConfig` 參數 

調用的控制器返回 json格式的數據即可，對json裡面的數據沒有要求

如在控制器中添加一個方法:

```
public function actionRemoveImage($id){
    Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
    Yii::$app->response->data = [
        'status' => true
    ];
}
```


参考: [How add one more button to yii2-widget-fileinput?](https://github.com/kartik-v/yii2-widget-fileinput/issues/83)

* yii2 下載文件

使用 yii2的sendfile方法

```
function actionDownload()
{
    $imgFullPath = 'picture.jpg';
    return Yii::$app->response->sendFile($imgFullPath);
}
```

* yii2 實現類似 required_if 的功能


[Yii2: Conditional Validator always returns required](https://stackoverflow.com/a/28934168)

* 词 `number spinner` 数值微调器

* yii2 Select2 Widget 二級聯動

參考

1. [krajee Select2 Settings](http://demos.krajee.com/widget-details/select2#settings) 裡面有 `pluginEvents`
2. [select2 plugin Add, select, or clear items](https://select2.org/programmatic-control/add-select-clear-items#creating-new-options-in-the-dropdown) select2 中添加元素和清空選項
3. [Yii2.0 实现三级联动](https://www.yiichina.com/tutorial/468) yii2的多級聯動參考

* yii2 grid view show footer

在 `girdview` 中添加 `'showFooter'=>true,`, gridview中就會顯示table footer部分，在需要顯示的列中，添加 `'footer'=> ''`,就會顯示對應的值

eg.

```
$total = 0;

foreach($dataProvider->models as $m) {
    if(is_numeric($m->price)){
        $total += $m->price;
    }
}

$showFooter = $total > 0?true:false;

```

```
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'showFooter'=>$showFooter,
    'columns' => [
        'id',
        'name',
        [
            'attribute' => 'price',
            'value' => function($model){
               return $model->price;
            },
            'footer' => number_format($total,2)
        ],
        [
            'header' => 'Action',
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
            'headerOptions' => ['width' => '80'],
        ],
    ],
]); ?>
```


参考: [Gridview with sum in footer](https://forum.yiiframework.com/t/gridview-with-sum-in-footer/83045/2)