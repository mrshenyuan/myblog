---
title: '使用sql对比Mysql中数据库2个表结构'
date: 2019-08-11 16:52:09
tags: 
  - mysql
  - copare
  - 对比
categories:
  - Mysql
description: '使用sql对比Mysql中数据库2个表结构'
---

比较两个数据表的结构

```
SELECT
	column_name,
	max(
		CASE
		WHEN table_name = 'table1' AND table_schema = 'db1'  THEN
			'Yes'
		END
	) AS in_table_1,
	max(
		CASE
		WHEN table_name = 'table1' AND table_schema = 'db2' THEN
			'Yes'
		END
	) AS in_table_2
FROM
	information_schema. COLUMNS
WHERE
	(
		(
			table_schema = 'db1'
			AND table_name = 'table1'
		)
		OR (
			table_schema = 'db2'
			AND table_name = 'table1'
		)
	)
AND table_name IN ('table1', 'table1')
GROUP BY
	column_name
ORDER BY
	column_name;
```

##### References:
1. [mysql: compare structure of two tables](https://stackoverflow.com/a/5470376/5840474)
2. [Query to compare the structure of two tables in MySQL](https://dba.stackexchange.com/a/75651)