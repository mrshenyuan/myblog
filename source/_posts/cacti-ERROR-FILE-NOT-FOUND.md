---
title: 'cacti ERROR: FILE NOT FOUND'
date: 2019-07-09 22:24:16
tags: 监控
categories:
- Linux
description: 解决在设置的Paths页面中提示FILE NOT FOUND的提示
---

Cacti 版本: 0.8a

在安装好 cacti之后，进入`Settings` -> `Paths`, 而且里面的路径在系统中都存在的，在这里显示`ERROR: FILE NOT FOUND`

![](/uploads/190709/1513240558.jpg)


参考1的博文，是 `php.ini` 中打开了`open_basedir`，受这个配置的影响

打开`php.ini`文件，在`open_basedir` 前加`;` 进行注释，然后重启apache,再刷新 cacti 设置的路径页面，就会变成绿色的了。


------

也可以在httpd.conf中vhost文件中加

```
php_admin_flag engine ON
php_admin_value open_basedir /var/www/html/:/tmp:/var/tmp
```

------

关于`open_basedir`

> 它可将用户访问文件的活动范围限制在指定的区域，
> 假设open_basedir=/home/wwwroot/home/web1/:/tmp/，那么通过web1访问服务器的用户就无法获取服务器上除了/home/wwwroot/home/web1/和/tmp/这两个目录以外的文件。
> 注意用open_basedir指定的限制实际上是前缀,而不是目录名。
> 举例来说: 若"open_basedir = /dir/user", 那么目录 "/dir/user" 和 "/dir/user1"都是可以访问的。所以如果要将访问限制在仅为指定的目录，请用斜线结束路径名。

关于符号链接(软链接)

>符号链接又叫软链接,是一类特殊的文件，这个文件包含了另一个文件的路径名(绝对路径或者相对路径)。
>路径可以是任意文件或目录，可以链接不同文件系统的文件。在对符号文件进行读或写操作的时候，系统会自动把该操作转换为对源文件的操作，但删除链接文件时，系统仅仅删除链接文件，而不删除源文件本身。

在开启了`open_basedir`，使用软链接，将非`open_basedir`的文件软链接到这个目录，也是无法操作的

如:

我的`open_basedir = /var/www/html/`

```ssh
touch /usr/bin/mytest.txt #创建一个open_basedir设定目录外的空文本文件
touch /var/www/html/mytest2.txt # 创建一个open_basedir设定目录内的空文本文件
ln -s /usr/bin/mytest.txt /var/www/html/mytest.txt #创建软链接
echo  "<?php echo file_get_contents('/var/www/html/mytest.txt');" > /var/www/html/mytest.php #创建 mytest.php
echo  "<?php echo file_get_contents('/var/www/html/mytest2.txt');" > /var/www/html/mytest2.php #创建 mytest2.php
php /var/www/html/mytest.php #执行php文件,运行结果:有提示错误
php /var/www/html/mytest2.php #执行php文件,运行结果:运行正常
```




##### References
1. [ltmp、lnmp下运行cacti监控path显示ERROR: FILE NOT FOUND](https://www.xiaohuai.com/4784)
2. [PHP绕过open_basedir限制操作文件的方法](https://www.jb51.net/article/141767.htm)
3. [open_basedir php授权目录设置](https://www.cnblogs.com/guohong-hu/p/9350076.html)