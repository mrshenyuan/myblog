---
title: '我自己的Mysql 笔记收集'
date: 2018-11-07 21:36:23
tags: 
  - diff
categories:
  - Mysql
description: '我自己的Mysql 笔记收集'
---

## mysql 比對數據表結構，生成更新sql語句 

1. [比较两个mysql数据库表结构的差异](https://blog.csdn.net/xlh1991/article/details/42011615)
2. [Navicat 比对两个数据库的差异](https://blog.csdn.net/qq_31156277/article/details/80410120)
3. [MySQL 对比数据库表结构](http://www.cnblogs.com/chenmh/p/5447205.html) 看評論，看到一個 `ampnmp.DatabaseCompare` 工具

------

## mysql 导出查询结果导出到文件

##### Reference
[MYSQL导入数据出现The MySQL server is running with the --secure-file-priv](https://www.cnblogs.com/deverz/p/9560616.html)
[MySQL 命令行导出、导入Select 查询结果](https://www.cnblogs.com/dee0912/p/3973339.html)
[mysql查询结果输出到文件](https://www.cnblogs.com/edgedance/p/7090800.html)

------

## mysql diff table and table data



##### Reference

1. [关于 phpMyAdmin 管理 Homestead MySQL 数据库的配置](https://learnku.com/articles/20987)
2. [Mysql 对比两张表数据](https://blog.csdn.net/gaochen519/article/details/51862681)
3. [mysqldiff](https://www.cnblogs.com/gelu/p/9510897.html)
4. [mysqldiff实现MySQL数据表比较](https://blog.csdn.net/lanwilliam/article/details/78664620)
5. [MySql 查询数据库中所有表名](https://www.cnblogs.com/BenWong/p/3996061.html)