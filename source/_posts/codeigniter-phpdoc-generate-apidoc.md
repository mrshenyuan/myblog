---
title: "Codeigniter项目使用phpDocumentor生成api文档"
date: 2019-11-20 20:10:56
tags: 
  - Codeigniter
  - apidoc
categories:
  - Codeigniter
description: "Codeigniter项目使用phpDocumentor生成api文档"
---


## 前言

运行环境: 

1. vagrant 2.2.4

2. virtualbox 6.0 
3. box `bento/ubuntu-16.04`  (Apache 2.4.18 + Mysql 5.7.26 + PHP 5.6.40)
4. [phpDocumentor](https://www.phpdoc.org/) 

 phpDocumentor 2  是一个尽可能的通过你的PHP源码来生成文档的工具。

通过 Codeigniter项目applicaiton下控制器和模型 2个文件夹的源码来生成 api文档

```
vagrant@vagrant:~$ phpdoc -V
phpDocumentor version v2.9.0
```

我是通过 [phar]( https://docs.phpdoc.org/getting-started/installing.html#phar ) 方式进行安装

```
wget https://www.phpdoc.org/phpDocumentor.phar # 下载phpDocumentor.phar文件
# php phpDocumentor.phar -V
sudo mv phpDocumentor.phar /usr/local/bin/phpdoc #将文件移入到/usr/local/bin下，并重命名为phpdoc,在所有目录下，都可以运行 phpdoc命令了，即全局命令
phpdoc -h # 输出帮助信息
phpdoc -V # 输出版本信息
phpdoc template:list # 列出使用的模板
```

## 使用

简单的使用 phpDocumentor  是使用命令行参数(`-d` 一个目录，`-f` 一个文件)来提供一个输入点和告诉它你喜欢放的文件夹(`-t`)来输出你的文档。

例如:

```
$ phpdoc -d ./src -t ./docs/api
```

 上面的案列会扫描 在`src` 和它子文件夹下的所有文件，执行分析和生成一个包含文档的网站在`docs/api`下。如果你没有指定`-t`选项，文档的输出将会写入到一个子文件夹为`output`文件夹中。

命令:

```
codeignniter$: phpdoc -d "application/controllers,application/models" -t ./docs/api
```

在`docs/api`可以看到生成的文档



通过[配置](https://docs.phpdoc.org/references/configuration.html)文件进行配置

`phpdoc.dist.xml` 配置文件内容

```
<?xml version="1.0" encoding="UTF-8" ?>
<phpdoc>
    <title>Codeigniter</title>
    <parser>
        <target>docs/api</target>
    </parser>
    <files>
        <directory>application/controllers</directory>
        <directory>application/models</directory>
    </files>
    <transformations>
        <template name="responsive-twig" />
        <target>docs/api</target>
    </transformations>
</phpdoc>
```

命令:

```
codeigniter$: phpdoc -c phpdoc.dist.xml
```

------

[apigen](http://www.apigen.org/)

我安裝apigen網站的教程，通過 composer的方式安裝 `apigen` 庫，然後生成基於Codeigniter 3.1.10項目的Api 文檔

```
composer require apigen/apigen --dev
```
composer.json 中`require-dev`顯示的 `"apigen/apigen": "^4.1"`

單個源文件夾`src`,輸出目錄`docs`

```
vendor/bin/apigen generate -s application/controllers --destination docs2
```

將應用的

```
vendor/bin/apigen generate -s application/controllers,application/models,application/core,application/helpers --destination apigendoc
```

apigen generate help

```
vagrant@vagrant:/var/www/html/codeigniter310$ vendor/bin/apigen generate --help
Usage:
  generate [options]

Options:
  -s, --source=SOURCE                        Dirs or files documentation is generated for. (multiple values allowed)
  -d, --destination=DESTINATION              Target dir for documentation.
      --access-levels=ACCESS-LEVELS          Access levels of included method and properties. [default: ["public","protected"]] (multiple values allowed)
      --annotation-groups=ANNOTATION-GROUPS  Generate page with elements with specific annotation.
      --base-url=BASE-URL                    Base url used for sitemap (useful for public doc).
      --config=CONFIG                        Custom path to apigen.neon config file. [default: "/var/www/html/mainpowers3/apigen.neon"]
      --google-cse-id=GOOGLE-CSE-ID          Custom google search engine id (for search box).
      --google-analytics=GOOGLE-ANALYTICS    Google Analytics tracking code.
      --debug                                Turn on debug mode.
      --deprecated                           Generate documentation for elements marked as @deprecated
      --download                             Add link to ZIP archive of documentation.
      --extensions=EXTENSIONS                Scanned file extensions. [default: ["php"]] (multiple values allowed)
      --exclude=EXCLUDE                      Directories and files matching this mask will not be parsed (e.g. */tests/*). (multiple values allowed)
      --groups=GROUPS                        The way elements are grouped in menu. [default: "auto"]
      --charset=CHARSET                      Charset of scanned files. (multiple values allowed)
      --main=MAIN                            Elements with this name prefix will be first in tree.
      --internal                             Include elements marked as @internal.
      --php                                  Generate documentation for PHP internal classes.
      --skip-doc-path=SKIP-DOC-PATH          Files matching this mask will be included in class tree, but will not create a link to their documentation. (multiple values allowed)
      --no-source-code                       Do not generate highlighted source code for elements.
      --template-theme=TEMPLATE-THEME        ApiGen template theme name. [default: "default"]
      --template-config=TEMPLATE-CONFIG      Your own template config, has higher priority templateTheme.
      --title=TITLE                          Title of generated documentation.
      --todo                                 Generate documentation for elements marked as @todo.
      --tree                                 Generate tree view of classes, interfaces, traits and exceptions.
  -h, --help                                 Display this help message.
  -q, --quiet                                Do not output any message.
  -V, --version                              Display this application version.

Help:
  Generate API documentation
```



## References

* [ApiGen4.1 windows安装教程](https://www.cnblogs.com/ruionline/p/4861077.html)
* [使用APIGEN自动生成PHP文档](https://www.greatqq.com/2016/04/php-apigen-document/)