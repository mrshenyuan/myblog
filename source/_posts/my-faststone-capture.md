---
title: 'FastStone Capture 文件名设置小记录'
date: 2019-08-11 15:58:35
tags:
  - 'FastStone Capture'
categories:
  - 工具
description:  'FastStone Capture 文件名设置小记录'
---
我使用的FastStone Capture 的`文件名`设置

1. 文件名称模板 ` fs$Y$M$D#####@`
2. 起始于: [1] 重置 [ 选中] 新的一天自动重置为1


保存之后的图片名称如:`fs2019090400003y.jpg`