---
title: '配置 centos7 静态地址'
date: 2018-12-06 20:39:47
tags: 
  - ip
categories:
  - Linux
description: '配置 centos7 静态地址'
---

```
ip a show
```
显示目前所有的ip信息，我的是 `eth1`

进入`/etc/sysconfig/network-scripts/` 目录 你应该可以找到 `ifcfg-INTERFACENAME`的文件

打开需要编辑的文件，如我的: `sudo vim ifcfg-eth1`

将`BOOTPROTO=dhcp` 改为`BOOTPROTO=static`

设定ip信息，最终配置文件如:

```
BOOTPROTO=satic
ONBOOT=yes
PREFIX=24
DEVICE=eth1
IPADDR=192.168.1.184
NETMASK=255.255.255.0
GATEWAY=192.168.1.1
```

修改完之后，重启网络 `systemctl restart network`，最后通过`ip a show` 查看ip是否修改到,
#### 参考 ####
1. [How to configure a static IP address on CentOS 7 / RHEL 7](https://www.cyberciti.biz/faq/howto-setting-rhel7-centos-7-static-ip-configuration/)
2. [How to configure a static IP address in CentOS 7](https://www.techrepublic.com/article/how-to-configure-a-static-ip-address-in-centos-7/)
3. [Configure Static IP Address in CentOS](http://www.mustbegeek.com/configure-static-ip-address-in-centos/)
4. [[Centos7]Vagrantでpublic networkを使用して固定IPでアクセス可能にする](https://qiita.com/daikiojm/items/05c7b3ae7635ebec5d9c)
