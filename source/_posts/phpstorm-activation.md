---
title: phpStorm 激活
date: 2019-07-28 13:16:59
tags:
  - ide
  - phpstorm
categories:
  - 工具
description: phpStorm 激活
---

[IntelliJ IDEA 注册码](http://idea.lanyus.com/)

1. 修改自己电脑的host 文件,添加


```
0.0.0.0 account.jetbrains.com
0.0.0.0 www.jetbrains.com
```

2. 点击页面的`获取注册码`按钮，,将弹出的注册码复制下来，粘贴到 phpStorm的注册页面的`Activation Code`选项的输入框中，点击`OK` 就激活成功了

