---
title: 学习 Ubuntu apache2 开启 https 服务
date: 2018-04-19 23:00:39
tags: [ubuntu,apache2,https]
description: 学习在Ubuntu Server中的apache2 开启 https 服务
---

系统: Ubuntu Server 16.04
Ubuntu中已安装软件: apache2,mod_ssl,openssl

是自己的私钥，证书，然后 应用到Apache2中，所有操作在 `/etc/apache2` 文件夹下

1. 在 /etc/apache2 目录下新建一个名为`ssl`的文件夹

```
sudo mkdir -p /etc/apache2/ssl
# 进入 apache2下的ssl文件夹
cd /etc/apache2/ssl
```

2. 进入 `/etc/apache2/ssl` 文件夹下 ，通过openssl 生成服务器密钥和公钥，服务器证书

```
# 生成 服务器密钥
sudo openssl genrsa -out server.key 1024 
# 生成 服务器公钥
sudo openssl req -new -key server.key -out server.csr
# 建立 服务器证书，天数是 100 年，可按实际要求修改
sudo openssl x509 -req -days 36500 -in server.csr -signkey server.key -out server.crt 
```

3. 修改 apache2 的 `default-ssl.conf` 文件

```
# 编辑 apache 默认的ssl 配置文件
sudo vim sites-available/default-ssl.conf
```

替换 `SSLCertificateFile`,`SSLCertificateKeyFile` 的值 类似于

```
#SSLCertificateFile     /etc/ssl/certs/ssl-cert-snakeoil.pem
#SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
SSLCertificateFile     /etc/apache2/ssl/server.crt
SSLCertificateKeyFile /etc/apache2/ssl/server.key
```

4. 启用 apache的 SSL 模块和SSL 站点
如果已启用过，可忽略本步骤

```
# 启用 SSL 模块
sudo a2enmod ssl
# 启用 SSL 站点
sudo a2ensite default-ssl
```

5. 重启 apache2
```
sudo service apache2 restart
```

最后尝试使用 https的形式访问 apache,如: [https://localhost](https://localhost) , chrome 打开可能会提示 `您的连接不是私密连接`，点击 `高级`, 点击 `继续前往localhost(不安全)`，应该就可以看到 apache2的首页了. 自签名证书浏览器一般会提示不合法 :see_no_evil:

### 参考文献 ###
1. [apache和nginx开启https](https://blog.csdn.net/zph1234/article/details/52316829)
2. [Ubuntu下配置apache开启https](https://www.cnblogs.com/IT--Loding/p/6071855.html)