---
title: 'centos apache You don not have permission to access / on this server'
date: 2018-12-06 21:10:47
tags: 
  - apache
  - centos
categories:
  - 运维
description: 'centos apache You don not have permission to access / on this server'
---

1. 关闭 selinux
2. 配置 `/etc/httpd/conf/httpd.conf` 中的两个目录添加 `Allow from all`

最后修改完配置，需要重启 apache

```
sudo systemctl restart httpd
```

参考: [You don't have permission to access / on this server](https://stackoverflow.com/a/17543828)
