---
title: 'Yii2 : Active Record add Not In condition'
date: 2018-11-13 21:49:09
tags: 
  - codition
  - ActiveRecord
categories:
  - Yii2
description: 'Yii2 : Active Record add Not In condition'
---

```
$query = MyModel::find()->where(['not in','attribute',$array]);
```
#### 參考 ####
1. [Yii2 : Active Record add Not In condition](https://stackoverflow.com/a/25952316)
2. [Adding a custom WooCommerce product type](https://jeroensormani.com/adding-a-custom-woocommerce-product-type/)
3. [How to Set Up WooCommerce Demo Data](https://shopplugins.com/woocommerce-demo-data/)