---
title: 'Incorrect username or password ( access token )'
date: 2019-01-31 21:09:47
tags: 
categories:
  - Git
description: 'Incorrect username or password ( access token )'
---

git登录是用户名正确了密码给输入错误了，以为提交的时候会弹出登录窗口，结果这玩意不提示了，直接来个用户名或密码错误，remote: Incorrect username or password ( access token ) fatal: Authentication failed for ~


解决方案是：打开电脑的控制面板–>用户账户–>管理Windows凭据(也可以直接输入：控制面板\用户帐户\凭据管理器)，win10可以直接查找。也可以直接搜索`凭据管理器`

"Windows 凭据"

找到普通凭据中自己的账号信息，选择编辑，填入正确的用户名和密码，最后点击保存即可或者删除，提交时，再输入一次正确的用户名和密码。

##### References
1. [https://www.cnblogs.com/allyh/p/10416066.html](https://www.cnblogs.com/allyh/p/10416066.html)