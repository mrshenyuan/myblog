---
title: 'CentOS 7 禁用 selinux'
date: 2018-12-06 20:50:47
tags: 
  - selinux
  - 防火墙
categories:
  - Linux
description: 'CentOS 7 禁用 selinux'
---

```
sudo vim /etc/selinux/config
```

将 SELINUX=enforcing改为SELINUX=disabled 
设置后需要重启才能生效

参考: [CentOS 7.X 关闭SELinux](https://www.cnblogs.com/activiti/p/7552677.html)

---- 

如果遇到 `centos apache You don't have permission to access / on this server`

1. 关闭 selinux
2. 配置 `/etc/httpd/conf/httpd.conf` 中的两个目录添加 `Allow from all`

最后修改完配置，需要重启 apache

```
sudo systemctl restart httpd
```

参考: [You don't have permission to access / on this server](https://stackoverflow.com/a/17543828)
