---
title: 'PHP 获取文件mime'
date: 2019-03-19 21:09:47
tags: 
  - mime
categories:
  - PHP
description: 'PHP 获取文件mime'
---

代码:

```
$finfo = finfo_open(FILEINFO_MIME_TYPE);
echo  finfo_file($finfo, 'path/to/demo.mp4');
finfo_close($finfo);
```

输出:

```
video/mp4
```

------

在页面中使用了 `Web Uploader` 的进行分片上传时，如果是上传大文件的话,Lumen 接收之后，通过 `$file->getClientMimeType()` 获取分片的类型时，获取的 mime 是 `application/octet-stream`,在合并分片之后，需要再次获取合并文件的 mime 类型.(因为需要获取到上传文件的mime类型)

##### Reference #####
1. [How to get MIME type of a file in PHP 5.5?](https://stackoverflow.com/a/23287361)
