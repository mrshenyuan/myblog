---
title: Yii2 使用 npm 安装的包
date: 2019-07-28 13:17:33
tags: 
  - Yii2
categories:
  - Yii2
description: Yii2 使用 npm 安装的包
---

转载自: [yii2.0.15 使用 npm 替换 bower，加速 composer 安装速度 [ 2.0 版本 ]](https://www.yiichina.com/tutorial/1676)

1. 修改 `ommon/config/main.php`

```
<?php
return [
'aliases' => [
            '@bower' => dirname(dirname(__DIR__)) . '/node_modules',
            '@npm' => dirname(dirname(__DIR__)) . '/node_modules',
        ],
];
```

这样类似 BootstrapPluginAsset 的 public $sourcePath = '@bower/bootstrap/dist'; 就会正确定位到 path/to/project/node_modules。

新增、修改  package.json，将 yii2 的 composer 依赖 `bower-asset/*` 转换为对应的 npm 包：

```
{
  "private": true,
  "dependencies": {
    "jquery": "^2.2.4",
    "bootstrap": "3.3.7",
    "inputmask": "^3.3.11",
    "jquery-treegrid": "^0.3.0",
    "jquery-ui": "^1.12.1",
    "punycode": "^2.1.0",
    "typeahead.js": "^0.11.1",
    "yii2-pjax": "^2.0.7"
  },
  "devDependencies": {},
  "license": "BSD-3-Clause"
}
```

再修改 `composer.json`

```
{
    "provide": {
        "bower-asset/jquery": "*",
        "bower-asset/bootstrap": "*",
        "bower-asset/inputmask": "*",
        "bower-asset/punycode": "*",
        "bower-asset/typeahead.js": "*",
        "bower-asset/yii2-pjax": "*"
    },
    "scripts": {
        "post-install-cmd": [
            "yii\\composer\\Installer::postInstall",
            "yarn install"
        ],
        "post-create-project-cmd": [
            "yii\\composer\\Installer::postCreateProject",
            "yii\\composer\\Installer::postInstall",
            "yarn install"
        ]
    }
```

删除项目根目录下的 vendor 和 node_modules 文件夹后，将 composer 和 npm 都设置为使用国内的镜像源，执行：

```
rm composer.lock # remove composer.lock if exist
composer install
```


composer.json
```
    "config": {
        "process-timeout": 1800,
        "fxp-asset":{
            "installer-paths": {
                "npm-asset-library": "vendor/npm",
                "bower-asset-library": "vendor/bower"
            }
        }
    },
```

------
删除composer全局安装的包

[How to remove globally a package from Composer?](https://stackoverflow.com/a/19223230)

```
composer global remove <packagename>
```

如:

```
composer global remove fxp/composer-asset-plugin
```