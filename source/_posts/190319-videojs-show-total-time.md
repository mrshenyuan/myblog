---
title: 'Video.js 显示总时间'
date: 2019-03-18 21:29:47
tags: 
  - videojs
categories:
  - javascript类库
description: 'Video.js 显示总时间'
---

添加css样式

```
.video-js .vjs-time-control{display:block;}
.video-js .vjs-remaining-time{display: none;}
```

##### Reference #####
1. [Show the current time of the video, instead of the remaining time on videojs](https://stackoverflow.com/a/44350248)