---
title: 'Wordpress 获取自定义文章类型的文章列表'
date: 2019-03-21 20:16:33
tags: 
  - post
categories:
  - Wordpress
description: 'Wordpress 获取自定义文章类型的文章列表'
---


```
$args = array( 
  'numberposts'		=> -1, // -1 is for all
  'post_type'		=> 'movie', // or 'post', 'page' 文章类型
  'orderby' 		=> 'ID', // or 'date', 'rand' 
  'order' 		=> 'ASC', // or 'DESC'
);

// Get the posts
$myposts = get_posts($args);
if($myposts){
    foreach($myposts as $mypost){
        echo $mypost->ID.' '.get_the_title($mypost->ID);
    }
    wp_reset_postdata();
}
```

##### Reference #####
1. [SHOWING A LIST OF CUSTOM POST TYPE USING GET_POSTS() LOOP IN WORDPRESS](http://blog.netgloo.com/2014/08/27/showing-a-list-of-custom-post-type-using-get_posts-loop-in-wordpress/)