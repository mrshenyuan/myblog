---
title: Symfony2 css/js 管理
date: 2015-08-12 08:56:09
tags: note
description: Symfony2 css/js 管理
---

直接使用某个css/js文件:

```
{{ asset('xxx.js')}}
```

注册/定义资源文件

```
{% javascripts %} <!-- global js -->
{% stylesheets %} <!-- global css -->
{% endjavascripts %}

{% javascripts '@HelloWebBundle/Resources/public/js/*' %}
    <script type="text/javascript" src="{{ asset_url }}"></script>
{% endjavascripts %}

{% stylesheets '@HelloWebBundle/Resources/public/css/*' %}
    <link rel="stylesheet" href="{{ asset_url}}" />
{% endstylesheets  %}
```


可以用global css,global js来进行管理

建立两个global块


coffee script

```
{% javascripts '@HelloWebBundle/Resources/public/js/index/*.coffee' filter="coffee" %}
    <script type="text/javascript" src="{{ asset_url }}"></script>
{% endjavascripts %}
```


config.xml

```
assetic:
    filter:
        coffee:
            bin: /usr/bin/local/coffee
            node: /usr/bin/node 
```


7-7 代码的压缩和优化

也是filter

uglifyjs2

```
filter:

    uglifyjs2:
       bin: /usr/bin/uglifyjs2
```

如果在filter="coffee,?uglifyjs2",是只在生产环境中使用，在开发环境中，不压缩

7-8 版本控制

config.xml
 
```
framework:
      templating:
               assets_version:%%5
               assets_version_format:%%s?version=%%
```


生产环境中:预编译,不是实时编译

使用命令

```
php app/console assetic:dump --env=prod --no-debug
```



分享:

   多线程进行资源文件dump

   asseticBundle

   修改composer.json中的assetic-bundle的版本为2.5.0