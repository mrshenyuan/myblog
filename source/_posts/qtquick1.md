---
title: qtquick 学习笔记和其它记事
date: 2016-08-25 09:54:04
tags: 
description: qtquick 学习笔记和其它记事
---

这里将记录我自己个人收藏的其它程序员的资料(网站)

SEO方面的:
索非亚SEO博客:http://seo.qiankoo.com
---
Qt Quick 简单教程
原文地址](http://blog.csdn.net/foruok/article/details/28859415)
---
**Rectangle**
1. 渐变色
QML 中渐变色的类型是 Gradient ，渐变色通过两个或多个颜色值来指定， QML 会自动在你指定的颜色之间插值，进行无缝填充。Gradient 使用 GradientStop 来指定一个颜色值和它的位置（取值在 0.0 与 1.0 之间）。
好吧，无码不欢，快快看一个示例：
```javascript
  Rectangle {
    width: 100; 
    height: 100;
    gradient: Gradient {
        GradientStop { position: 0.0; color: "#202020"; }
        GradientStop { position: 0.33; color: "blue"; }
        GradientStop { position: 1.0; color: "#FFFFFF"; }
    }
}
```
```javascript
Rectangle {
    width: 100; 
    height: 100;
    rotation: 90;
    gradient: Gradient {
        GradientStop { position: 0.0; color: "#202020"; }
        GradientStop { position: 1.0; color: "#A0A0A0"; }
    }
}
```
2. Item
3. Text
    Text 元素可以显示纯文本或者富文本（使用 HTML 标记修饰的文本）。它有 font / text / color / elide / textFormat / wrapMode / horizontalAlignment / verticalAlignment 等等属性，你可以通过这些属性来决定 Text 元素如何显示文本。
  当不指定 textFormat 属性时， Text 元素默认使用 Text.AutoText ，它会自动检测文本是纯文本还是富文本，如果你明确知道要显示的是富文本，可以显式指定 textFormat 属性。 
```javascript
   import QtQuick 2.0
Rectangle {
    width: 300;
    height: 200;
    Text {
        width: 150;
        height: 100;
        wrapMode: Text.WordWrap;
        font.bold: true;
        font.pixelSize: 24;
        font.underline: true;
        text: "Hello Blue Text";
        anchors.centerIn: parent;
        color: "blue";
    }
}
```
 Text 元素的 style 属性提供了几种文字风格，Text.Outline 、 Text.Raised 、 Text.Sunken ，可以使文字有点儿特别的效果。而 styleColor 属性可以和 style 配合使用（如果没有指定 style ，则 styleColor 不生效），比如 style 为 Text.Outline 时，styleColor 就是文字轮廓的颜色。看个简单的示例：
```javascript
import QtQuick 2.0
Rectangle {
    width: 300;
    height: 200;
    Text {
        id: normal;
        anchors.left: parent.left;
        anchors.leftMargin: 20;
        anchors.top: parent.top;
        anchors.topMargin: 20;
        font.pointSize: 24;
        text: "Normal Text";
    }
    Text {
        id: raised;
        anchors.left: normal.left;
        anchors.top: normal.bottom;
        anchors.topMargin: 4;
        font.pointSize: 24;
        text: "Raised Text";
        style: Text.Raised;
        styleColor: "#AAAAAA" ;
    }
    Text {
        id: outline;
        anchors.left: normal.left;
        anchors.top: raised.bottom;
        anchors.topMargin: 4;
        font.pointSize: 24;
        text: "Outline Text";
        style: Text.Outline;
        styleColor: "red";
    }
    Text {
        anchors.left: normal.left;
        anchors.top: outline.bottom;
        anchors.topMargin: 4;
        font.pointSize: 24;
        text: "Sunken Text";
        style: Text.Sunken;
        styleColor: "#A00000";
    }
}
```
这个示例除了用到 Text 元素，还使用 anchors 来完成界面布局。
Text 就介绍到这里，下面看 Button 。

---

20150112 Ubuntu server 处理小记

##### 题外话

   今天上班,算是很悠闲,没有状态的工作,你懂的,接近过年了嘛,今天有两位同事已经放假了(公司目前只有4人),就剩下我和一位android同事了.
   
##### 正文

上午用putty.exe用ssh登录远程服务器时,使用的是root用户,开始执行了

```shell
chmod 777 /* -R
```

运行一会之后,我回到根目录(/)时,ls -l 之后,列出根目录下的所有目录,全是绿色的,777权限了,我想,这样非常危险,所以手贱的执行了

```sheel
chmod 666 /* -R
```

然后我再使用: ls 命令之后,显示

```sheel
root@localhost:ls
root@localhost:permission denied
```

连"ls"命令都用不了了,有一个博友告诉我可以用"single 模式"进行恢复,可以那是远程的服务器,我只好求助于老板帮忙处理.在12:29分时,说需要重新安装,中午由于在大鱼塘中吃了猪手,聊了一会天,回公司已是14:10多分了.然后我访问服务器的网站,可以正常访问了.测试了我们PHP组做的一些项目.都基本可以运行了,mysql和项目都在.但是里面有一个java项目,我使用非root登录进入,输入用户名密码之后,在命令窗口中输入:

```shell
root@localhost:java
root@localhost:- none such file or directory
```

是的,没有安装jdk,老板叫我安装jdk,后来我回了"我处理可能需要久一些,因为从oracle.com中下载jdk比较慢",老板告诉我可以叫香港的同事,帮忙下载,是的.我找了项目经理帮忙下载32位jdk1.7.0_25-linux-i586.gz,通过之前我配置过的jdk收藏,重新配置了一次jdk到"~/.bashrc"中."source ~/.bashrc"之后,.我输入

```shell
root@localhost:java
root@localhost:Failed to extract the files
```

通过搜索:"Ubuntu jdk Failed to extract the files",找到了
[在Ubuntu下安装jdk解压出现问题：./jdk-6u30-linux-i586.bin: 113: ./install.sfx.3631: not found](http://blog.csdn.net/weiqifa0/article/details/11564961)的文章,然后我是使用:

```shell
root@localhost:uname -a
root@localhost:Linux localhost-118 3.18.5-x86_64-linode52 #1 SMP Thu Feb 5 12:18:36 EST 2015 x86_64 x86_64 x86_64 GNU/Linux
```

我才知道jdk的位数不正确.重新安装一次之后,我是将JDK的资料配置到"~/.bashrc"中,输入"java"之后,会出现很多信息,说明jdk安装成功了.
晚上回宿舍,由于担心我登录root用户之后,像上午误操作,我使用非root用户,进入tomcat6的目录,输入

```shell
root@localhost:sudo ./bin/startup.sh
root@localhost:user is not in the sudoers file.
```

查询之后,发现是用户未配置到sudoers文件中,我是参考[user is not in the sudoers file.](http://my.oschina.net/u/699015/blog/262364)处理好,将非root用户添加进去了.

*JDK最后配置的位置放在~/.bashrc,  /etc/profile,  tomcat的startup.sh和shutdown.sh中,我目前还未清楚有什么分别,留着日后来找资料*

我配置tomcat的jdk是参考[Ubuntu 配置 Tomcat ](http://blog.csdn.net/ithomer/article/details/7827045)来进行配置的,配置在tomcat6/bin下的startup.sh和shutdown.sh两个文件中的.
在tomcat6中,进入logs目录,可以使用:

```shell
tail -f catalina.out
```

命令来实时看见tomcat输出的运行信息.
进入tomcat6的bin目录,我一般是执行停止服务,启动服务的命令

```shell
sh ./shutdown.sh
ps -ef|grep java
sh ./startup.sh
```

来进行重启tomcat的操作