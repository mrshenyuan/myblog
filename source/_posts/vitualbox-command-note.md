---
title: 'virtual box command note'
date: 2019-08-11 21:35:42
tags: 
  - 命令
categories:
  - 零碎笔记
description: 'virtual box command note'
---

Reference [How can I tell if a virtual machine is up from the command line?](https://superuser.com/a/934044)

1. list virtualbox vm

```
vboxmanage list vms
VBoxManage list runningvms 
```

2. command run virtualbox virtual machine

```
VBoxManage startvm "VM name" --type headless
```

eg.

```
VBoxManage startvm "jira2" --type headless
VBoxManage startvm confluence --type headless
```