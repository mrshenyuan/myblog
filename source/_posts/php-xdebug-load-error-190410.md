---
title: 'PHP Warning:  Xdebug MUST be loaded as a Zend extension in Unknown on line 0'
date: 2019-04-10 22:50:47
tags: 
  - Xdebug
categories:
  - PHP
description: 'PHP Warning:  Xdebug MUST be loaded as a Zend extension in Unknown on line 0'
---

将 `php.ini`配置文件中的

```
extension=xdebug.so
```

修改为

```
zend_extension=xdebug.so
```

修改完`php.ini`文件，不要忘记重启 web服务器.

##### Reference 
1. [PHP Warning: Xdebug MUST be loaded as a Zend extension](https://stackoverflow.com/a/26222131)