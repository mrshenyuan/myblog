---
title: '查看 centos版本'
date: 2019-08-11 16:34:55
tags:
  - centos
  - version
  - 版本
categories:
  - 运维
---

查看 centos版本 [How to Check CentOS Version](https://www.thegeekdiary.com/how-to-check-centos-version/)

```
cat /etc/redhat-release
cat /etc/centos-release
cat /etc/os-release
```