---
title: 看Symfony2关于Doctrine2的笔记
date: 2015-08-12 08:56:59
tags: note
---

对框架中操作数据的几个基本需求
1. 安全
2. 性能
3. ORM (Object-Relational Mapper) 
4. 支持多种数据库
5. 支持多种第三方插件
