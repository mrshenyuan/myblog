---
title: docker gitlab backup
date: 2018-11-12 22:30:17
tags:
  - docker
  - gitlab
  - backup
categories:
  - 运维
description: docker gitlab backup
---

说明:下面命令中带有`<your container name>`字，是 gitlab 容器的名称，请按实际情况进行代替



在创建备份之前，你不需要停止任何东西

```
docker exec -t <your container name> gitlab-rake gitlab:backup:create
```

`gitlab_backup.sh` content:

```
#！ /bin/bash
case "$1" in 
    start)
        docker exec -t <your container name> gitlab-rake gitlab:backup:create
        ;;
esac
```

嘗試腳本是否可以正常運行

```
gitlab_backup.sh start
```

將腳本放入到 /root下，並使用 `sudo crontab -e` 添加一下內容

```
0 2 * * * /root/gitlab_backup.sh start
```

重啟cron服務

```
sudo service cron reload
sudo service cron restart
```



-----



进入gitlab容器:

```
#来自Gitlab docker外的Ubuntu主机
sudo docker exec -it <your container name> /bin/bash
```

查看gitlab 本地的备份路径

`gitlab_rails['backup_path']` 在 Gitlab 配置文件 `gitlab.rb` 是 注释的, 它的默认备份路径是在 `/var/opt/gitlab/backups`.

```
# 来自Gitlab Docker
root@bcc3d4829e2c:/# cat /etc/gitlab/gitlab.rb | grep backup_path
# gitlab_rails['manage_backup_path'] = true
# gitlab_rails['backup_path'] = "/var/opt/gitlab/backups"
```


### 创建一份备份 ###


### 检测备份 ###

```
# 来自Gitlab Docker
root@bcc3d4829e2c:/etc/gitlab# ls -lart /var/opt/gitlab/backups
```

### 备份配置文件和 密钥文件 ###

在上面创建备份时，没有备份 配置文件和密钥文件，这是因为上面一步是使用 [ 使用密钥文件加密Gitlab 数据](https://docs.gitlab.com/ee/raketasks/backup_restore.html#storing-configuration-files)，如果你将他们保存相同的地方，你将会破坏加密过程。

所以请备份 `/etc/gitlab/gitlab.rb` 和`/etc/gitlab/gitlab-secrets.json` 并且将他们保存在一个其它Gitlab 备份数据的安全的地方


### 还原 Gitlab ###


你只能通过Gitlab 备份还原 一样的Gitlab版本和类型。并且你需要一个运行中的Gitlab实例。

### 停止一些 Gitlab 服务 ###

```
# 来自Gitlab Docker

gitlab-ctl reconfigure
gitlab-ctl start
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq
gitlab-ctl status
ls -lart /var/opt/gitlab/backups
```

### 开始还原 ###

备份文件一定可以在 定义在 Gitlab 配置文件`/etc/gitlab/gitlab.rb`的`gitlab_rails['backup_path']`的路径下找到

在 docker 外主机终端执行

```
#来自Gitlab docker外的Ubuntu主机
docker exec -it <your container name> gitlab-rake gitlab:backup:restore --trace
```

还原过程中会出现2次 `Do you want to continue (yes/no)?` 的询问操作，都输入`yes`之后，按`Enter` 回车键确认操作。



> 我们当然添加 `BACKUP` 参数来指定备份文件如果有多个备份文件在备份目录下，`BACKUP`参数的值是 备份文件的[时间戳](https://docs.gitlab.com/ee/raketasks/backup_restore.html#backup-timestamp)，比如: `docker exec -it <your container name> gitlab-rake gitlab:backup:restore BACKUP=1563732042_2019_07_21_11.1.4 --trace`

### 重启Gitlab并安全检查 ###

使用`gitlabctl restart`命令重启Gitlab服务

```
# 来自Gitlab Docker
gitlab-ctl restart
```



使用`gitlab-rake gitlab:check SANITIZE=true`命令来发起Gitlab安全检查

```
# 来自Gitlab Docker
gitlab-rake gitlab:check SANITIZE=true
```

使用`docker ps`来验证 Gitlab 容器健康

```
#来自Gitlab docker外的Ubuntu主机
sudo docker ps
```



#### 參考 ####

1. [docker部署的GitLab代码自动备份](https://blog.csdn.net/u014258541/article/details/79317180)
2. [Backup and restore Omnibus GitLab configuration](https://docs.gitlab.com/omnibus/settings/backups.html)
3. [Backup and restore Gitlab in docker](https://copdips.com/2018/09/backup-and-restore-gitlab-in-docker.html) 需要参考这个来写自己的尝试笔记