---
title: selenium学习使用
date: 2019-05-30 21:09:47
tags: 
  - selenium
categories:
  - 零碎笔记
description: selenium学习使用
---


##### References
1. [selenium前端自动化测试 一（node版本）](https://www.jianshu.com/p/53ccac537f97)
2. [Selenium 初探-Nodejs+Selenium环境搭建及基础用法](https://www.jianshu.com/p/0893e1d773ef)
3. [Selenium学习（二）-selenium命令（command）](https://www.cnblogs.com/lxoc/p/6716459.html)
4. [Python模拟浏览器实现用户响应](https://www.cnblogs.com/zhangchao3322218/p/5518379.html) 知道selenium
5. [Selenium "storeAttribute" example to store URL or any other attribute value of element node](http://www.software-testing-tutorials-automation.com/2013/06/selenium-storeattribute-example-to.html) `store Attribute`是保存匹配属性的值，`store Text`是保存匹配属性的文本