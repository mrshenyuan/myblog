---
title: 'php upload POST Content-Length of 1489003497 bytes exceeds the limit of 134217728 bytes'
date: 2019-03-01 21:09:47
tags: 
  - upload
categories:
  - PHP
description: 'php upload POST Content-Length of 1489003497 bytes exceeds the limit of 134217728 bytes'
---

修改 php.ini 文件

```
upload_max_filesize = 1000M;
post_max_size = 1000M;
```

##### Reference #####
1. [上传文件报错：Warning: POST Content-Length of 9443117 bytes exceeds the limit of 8388608 bytes in Unknown on line 0](https://www.cnblogs.com/zhylog/p/9929929.html)