---
title: '继承Laravel的SeedCommand'
date: 2019-05-05 22:08:47
tags: 
categories:
  - Laravel
description: '继承Laravel的SeedCommand'
---

Laravel: `5.5.*`

 `app/Console/Commands/CustomSeedCommand.php`



```
<?php

namespace App\Console\Commands;
use Illuminate\Database\Console\Seeds\SeedCommand;
use Illuminate\Database\DatabaseManager;


class CustomSeedCommand extends SeedCommand
{
    /**
     * CustomSeedCommand constructor.
     * @link https://stackoverflow.com/a/50680492
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        parent::__construct($databaseManager);
    }
}

```



如果没有构造方法，会提示类似一下的错误:



```
In Container.php line 918:

  Target [Illuminate\Database\ConnectionResolverInterface] is not instantiable while building [App\Console\Commands\CustomSeedCommand].

```



##### Reference

1. [Extending laravel's SeedCommand](https://stackoverflow.com/a/50680492)