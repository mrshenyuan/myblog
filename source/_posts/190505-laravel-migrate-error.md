---
title: 'laravel migrate Syntax error or access violation: 1071 Specified key was too long'
date: 2019-05-05 21:00:47
tags: 
categories:
  - Laravel
description: 'laravel migrate Syntax error or access violation: 1071 Specified key was too long'
---

Laravel: `5.5.*`

```
$ php artisan migrate
Migration table created successfully.

In Connection.php line 664:

  SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long; max key length is 1000
  bytes (SQL: alter table `users` add unique `users_email_unique`(`email`))


In Connection.php line 458:

  SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long; max key length is 1000
  bytes

```



使用的集成环境工具是`phpStudy 2018` PHP7.2.10 NTS +Apache

修改`config/database.php`中的

```
'charset' => 'utf8mb4',
'collation' => 'utf8mb4_unicode_ci',
```

改为

```
'charset' => 'utf8',
'collation' => 'utf8_unicode_ci',
```



然后数据库的数据格式也修改一下

##### Reference

1. [Laravel Migration Error: Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes](https://stackoverflow.com/a/46262128)
