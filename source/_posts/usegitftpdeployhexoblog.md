---
title: 使用git ftp发布hexo博客内容
date: 2016-09-25 21:40:52
tags: 工具
description: 使用git ftp工具发布hexo博客内容到虚拟主机中
---


原来hexo 部署支持ftp部署, 我看Hexo 文档的[Deployment](https://hexo.io/docs/deployment.html#FTPSync)也实现了一样的，哇哈

-----

   目前博客由[hexo3](http://hexo.io) + [next主题](https://github.com/iissnan/hexo-theme-next)，因为我想将 hexo 编译生成的文件可以通过ftp命令发布到ftp服务器上面。
 
发布使用的工具是[git-ftp](http://git-ftp.github.io/git-ftp/):
按照[Use Jenkins and git-ftp to deploy a website to shared webhosting](https://www.savjee.be/2016/02/Use-Jenkins-and-git-ftp-to-deploy-website-to-shared-webhosting/) 中的教程，我基本算完成了这个需要。

## 介绍

### 修改`站点配置文件`
`站点配置文件` `_config.yml` 是 位于站点根目录下，主要包含 Hexo 本身的配置.
```
# Directory
source_dir: source
public_dir: ../public ##修改输出目录
```
我这样修改，是为了博客编辑的目录(会同步到git 服务器中的)与博客编译之后(只在本地使用，用于git-ftp)需要发布到ftp的文件分开。

### git-ftp 安装方法
我是用按照git-ftp中的git方法安装的，命令是
```
git clone https://github.com/git-ftp/git-ftp.git
cd git-ftp
sudo make install
```

### 使用git-ftp 发布 hexo 编译之后的静态文件
因为我hexo 编译之后的 public文件夹是另外一个目录，所以我需要初始化`public`,运行的命令像:
```
git init
git add .
git commit -m "init"
```

这个工具有两个是我们感兴趣的:`init`和`push`,`init`命令是只用于第一次使用和上传你git仓库的所有文件到一个ftp服务器里面，并且它会创建一个小文件`.git-ftp.log`在服务器上面,它用于校验当前部署到服务器的提交(commit)。你可以运行它像:
```
git ftp init --user USERNAME --passwd PASSWORD ftp://YOUR-FTP-SERVER-ADDRESS/path/to/website/
```
在初始化 ftp 服务器之后你可以在git中开始一个新的提交(commit) 和 部署改变的文件，使用git-ftp的`push`命令：
```
git ftp push --user USERNAME --passwd PASSWORD ftp://YOUR-FTP-SERVER-ADDRESS/path/to/website/
```

总结：
使用git-ftp可以很方便的将git仓库中修改了的文件部署到ftp服务器上面.在部署之后,或许需要通过ftp工具，检查部署之后的文件夹是否发布到正确的位置。:smile: