---
title: ' php Jwt token 刷新'
date: 2019-03-27 20:08:47
tags: 
  - jwt
categories:
  - Laravel
description: ' php Jwt token 刷新'
---

Controller.php

```
<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Manager;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class Controller extends BaseController
{
 /**
     * @var JWTAuth
     */
    protected $jwt;

    /**
     * @var Manager
     */
    protected $manager;
    
    

    /**
     * Controller constructor.
     *
     * @param JWTAuth $jwt
     * @param Manager $manager
     */
    public function __construct(JWTAuth $jwt, Manager $manager)
    {
        $this->jwt = $jwt;
        $this->manager = $manager;
    }
```

refresh token

```
/**
* 刷新token
* @param Request $request
* @return mixed
*/
public function refreshToken(Request $request){
    try{
    	//get jwt token
        $old_token = $this->jwt->getToken();
        $token = $this->manager->refresh($old_token)->get();
    }catch (TokenExpiredException $e){
        return $this->json($e->getMessage(), 500);
    }catch(JWTException $e){
        return $this->json($e->getMessage(), 500);
    }
    return $this->json([
    	'token' => $token
    ]);
}
```

##### Reference #####
1. [how to refresh a token ? The token has been blacklisted ?](https://github.com/tymondesigns/jwt-auth/issues/831#issuecomment-252398536)