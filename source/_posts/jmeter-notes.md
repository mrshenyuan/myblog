---
title: 'jmeter使用杂记'
date: 2019-08-11 16:27:25
tags:
  - jmeter
categories:
  - 工具
description: 'jmeter使用杂记'
---


## jmeter 线程组之间传递变量

在请求中，右键 `添加` ->  `断言` -> `BeanShell断言`,

设置共享变量: `${__setProperty(name,value,)} `

在其他线程组中使用: `${__property(name)}`

参考: [Jmeter 线程之间传递变量](https://blog.csdn.net/jasonliujintao/article/details/71542021)

## jmeter 输出log

在 `BeanShell断言` 中，使用`log.info();` 输出log,就不用每次看 参看结果树了

参考: [JMeter学习-037-JMeter调试工具之四-BeanShell+jmeter.log](https://www.cnblogs.com/fengpingfan/p/5894031.html)

## 其它

1. jmeter 发送 application/json [Jmeter does not send JSON data in POST](https://stackoverflow.com/questions/37169153/jmeter-does-not-send-json-data-in-post)
2. jmeter 定义变量 [Jmeter添加变量的四种方法](https://www.cnblogs.com/jessicaxu/p/7512680.html)
3. jmeter 解析json [JMeter 插件 Json Path 解析HTTP响应JSON数据](https://blog.csdn.net/wangyunfeis/article/details/77712811)

插件地址: [Custom Plugins for Apache JMeter™](https://jmeter-plugins.org/?search=jpgc-json) version:2.6

4. jmeter 请求登录api，保存数据, [JMETER BASIC AUTHENTICATION EXPLAINED](https://octoperf.com/blog/2018/04/24/jmeter-basic-authentication/)
5. [JMeter中对于Json数据的处理方法](https://blog.csdn.net/wxyyxc1992/article/details/31069617)