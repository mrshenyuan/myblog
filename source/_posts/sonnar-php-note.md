---
title: sonnar php 笔记
date: 2017-07-09 19:51:22
tags: note
description: sonnar php 笔记
---

sonar scanner php 

1. [SonarQubeはオープンソースの静的解析ツール](http://qiita.com/kadoyau/items/70f4a403b42847e4ed00)
2. [SonarQube环境搭建与实践应用](http://www.jianshu.com/p/778fd35fd494)
3. [SonarQube for PHP project](http://sharpten.com/blog/2014/05/02/sonarqube_for_php_project.html)
4. [Analyse a Laravel 5 (PHP) project with SonarQube](https://geekalicious.pt/blog/en/continuous-integration/analyse-php-laravel-5-project-multilanguage-with-sonarqube)
5. [Installing SonarQube with Jenkins integration for a PHP project](http://cvuorinen.net/2013/07/installing-sonarqube-with-jenkins-integration-for-a-php-project/)

框架及框架的psr

https://www.searchenginejournal.com/guide-popular-php-frameworks-beginners/180922/


我的项目是基于Codeignier，在项目的根目录下新建`sonar-project.properties`文件

```
sonar.projectKey=org.codehaus.sonar:projectName
sonar.projectName=projectName
sonar.projectVersion=1.0.0
#这里是php文件放的地方 
sonar.sources=application/controllers,application/models,application/views,application/helpers
# Languagesonar.language=p 
hp 
#sonar.dynamicAnalysis=false 
# Encoding of the source files 
sonar.sourceEncoding=UTF-8
# D:/sonar-scanner/bin/sonar-scanner.bat  -Dsonar.login=6a7c6cc50973cf10f9d6621bfb8128a49764dac2
# D:/sonar-scanner/bin/sonar-scanner.bat sonar-scanner -Dsonar.projectKey=6a7c6cc50973cf10f9d6621bfb8128a49764dac2 -Dsonar.sources=.
# D:/sonar-scanner/bin/sonar-scanner.bat  -Dsonar.login=7bc5d1a744172be001b10df4225a1336b4dde5e3 #在命令行中执行的sonar-scanner
```