---
title: 'Centos7 安装 redis server'
date: 2018-12-06 20:50:47
tags: 
  - redis
categories:
  - 运维
description: 'Centos7 安装 redis server'
---

```
wget http://download.redis.io/releases/redis-5.0.4.tar.gz
tar xzf redis-5.0.4.tar.gz
cd redis-5.0.4
make
make install
cp /root/redis-5.0.4/src/redis.conf /etc/redis/6379.conf
cp /root/redis-5.0.4/utils/redis_init_script /etc/init.d/redisd
chkconfig redisd on

service redisd start #启动redis
service redisd stop # 停止redis
```

##### Reference #####
1. [CENTOS7下安装REDIS](https://www.cnblogs.com/zuidongfeng/p/8032505.html)