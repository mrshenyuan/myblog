---
title: 'Ubuntu Error: ENOSPC:System limit for number of file watchers reached'
date: 2019-02-27 22:00:47
tags: 
categories:
  - Linux
description: 'Ubuntu Error: ENOSPC:System limit for number of file watchers reached'
---

and design pro 運行 `npm start run` 

提示

```
Error: ENOSPC: System limit for number of file watchers reached
```

修改系统监控文件数量

```
sudo vim /etc/sysctl.conf
```

在末尾添加

```
fs.inotify.max_user_watches=524288
```
然後保存退出

最後執行:

```
sudo sysctl -p
```

##### Reference #####
1. [Ubuntu Error: ENOSPC:System limit for number of file watchers reached](https://blog.csdn.net/weixin_43760383/article/details/84326032)