---
title: yii2-php7.2-object
date: 2019-08-11 16:37:00
tags:
---

因为我的项目中的composer.json中的`"mdmsoft/yii2-admin": "~2.0"`, 需要修改 `vendor/mdmsoft/yii2-admin/components/Configs.php`

```
vi vendor/mdmsoft/yii2-admin/components/Configs.php
```

将`\yii\base\Object` 改为 `\yii\base\BaseObject` 就可以了
