---
title: 'ffMpeg Too many packets buffered for output stream 0:1'
date: 2019-03-20 20:58:49
tags: 
  - 转换
categories:
  - FFmpeg
description: 'ffMpeg Too many packets buffered for output stream 0:1'
---

出错的原因是:

有些视频数据有问题，导致视频处理过快，容器封装时队列溢出。

增大容器封装队列大小，比如

```
-max_muxing_queue_size 1024
```

如:

```
ffmpeg  -i test.mp4 -ss 00:00:00 -to 00:00:05 -max_muxing_queue_size 1024 -c:v libx264 -c:a copy  test_out.mp4 -y
```

以上的命令是截取 `test.mp4` 前5秒视频

##### Reference #####
1. [FFmpeg的那些坑-Too many packets buffered for output stream](https://blog.csdn.net/noahsun1024/article/details/80875460)