---
title: 'mysql 5.7 重置 root密码'
date: 2018-12-06 20:55:47
tags: 
  - 密码
categories:
  - Mysql
description: 'mysql 5.7 重置 root密码'
---

```
mysql -u root -p
```

```
UPDATE mysql.user SET authentication_string=PASSWORD('YOURPASSWOED') WHERE user='root' AND host='localhost';
flush privileges;
exit;
```

重启mysql

```
sudo systemctl restart mysqld
```

#### 参考 ####
1. [MySQL Server 5.7 修改 root 密码](https://www.jianshu.com/p/d9996e22603c)
2. [Reset MySQL 5.7 root password](https://www.techandme.se/reset-mysql-5-7-root-password/)