---
title: 'Laravel Illuminate\Http\Exceptions\PostTooLargeException'
date: 2019-07-28 14:59:21
tags: 
  - laravel
categories:
  - Laravel
description: 'Laravel Illuminate\Http\Exceptions\PostTooLargeException'
---

出错原因是: 请求的post的数据比 php.ini设定的 `post_max_size`大的原因

解决方法: 
增加`php.ini`中 `post_max_size`和`upload_max_filesize`的设置

打开 `/etc/php.ini` 文件

修改 2个字段，使它的值足够大,如:`1024M` 或者`2048M`

```
post_max_size = 1024M
upload_max_filesize = 1024M
```

生成环境上传文件大小的限制需按实际情况来设定

然后重启 web服务器(如:Apache)使配置生效