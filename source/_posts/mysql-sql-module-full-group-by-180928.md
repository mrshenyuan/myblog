---
title: 'mysql sql_module full_group_by'
date: 2019-08-11 21:38:36
tags: 
  - mysql
categories:
  - 零碎笔记
description: 'mysql sql_module full_group_by'
---


1.[mysqlを5.7にしたらsql_modeにonly_full_group_byが追加されてエラー](http://mng.seedcollector.net/blog/?p=787)

在 mysql的配置文件，如我是在 ubuntu 16.04中嘗試的，配置文件在 `/etc/mysql/mysql.conf.d/mysqld.cnf`,

```
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

在`[mysqld]`下添加

```
sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```