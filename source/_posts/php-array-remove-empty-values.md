---
title: php array remove empty values
date: 2018-11-13 21:49:31
tags: 
  - array
categories:
  - PHP
description: php array remove empty values
---

```
print_r(array_filter($linksArray));
```
#### 參考 ####

1. [Remove empty array elements](https://stackoverflow.com/a/3654309)
2. [Remove Empty Array Elements In PHP](https://www.thecodedeveloper.com/remove-empty-array-elements-in-php/)