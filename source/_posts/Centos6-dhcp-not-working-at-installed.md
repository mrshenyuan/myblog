---
title: Centos6 安装完之后，没有网络
date: 2019-07-09 23:06:04
tags: Centos 网络
categories:
- Linux
description: 处理Centos6 安装完之后，没有网络小记
---

Virtualbox安装的centos 6.10的虚拟机，安装时，网络是`NAT网络`，安装完之后，将网络改为`桥接网卡`，启动虚拟机之后，使用 `ifconfig` 命令查看没有到`eth0`的信息，只有`127.0.0.1`

编辑`/etc/sysconfig/network-scripts/ifcfg-eth0`文件

使用 `vi` 打开文件

```
vi /etc/sysconfig/network-scripts/ifcfg-eth0
```

我的`ifcfg-eth0` 文件的配置

```
DEVICE=eth0
HWADDR=08:00:27:3C:B5:84
TYPE=Ethernet
UUID=3e27d94d-9032-455f-b661-c050630df859
ONBOOT=yes # 配置文件只需要修动此处,将原先的no改为yes
NM_CONTROLLED=yes
BOOTPROTO=dhcp
```

然后重启网络

```
/etc/init.d/network restart #重启网络
ifconfig # 查看ip
```

##### References

1. [解决CentOS 6.5 安装之没有网络的问题 【转自云栖社区】](https://blog.csdn.net/gd6863/article/details/78513255)