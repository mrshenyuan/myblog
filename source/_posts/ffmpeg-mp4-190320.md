---
title: 'ffMpeg 转换格式为mp4'
date: 2019-03-20 20:50:47
tags: 
  - ffmpeg
  - mp4
categories:
  - FFmpeg
description: 'ffMpeg 转换格式为mp4'
---

```shell
ffmpeg -i huapai.mkv -c:v libx264 -crf 23 -c:a libfaac -q:a 100 -strict -2 huapai.mp4
````

```
ffmpeg  -i huapai.mkv -ss 00:00:00 -to 00:05:00 -c:v copy -c:a copy  huapai0500.mkv -y
```

```
ffmpeg -i huapai.mkv -ss 00:00:00 -to 00:05:00 -c:v libx264 -crf 23 -c:a libfaac -q:a 100 -strict -2 huapai2.mp4
```

##### Reference ######
1. [使用ffmpeg将视频转为x264编码的mp4文件](https://blog.csdn.net/yeyinglingfeng/article/details/78749322)
2. [ffmpeg mp4 to wmv and wmv to mp4](https://www.cnblogs.com/tianciliangen/p/5616905.html)
3. [ffmpeg转换视频 格式](https://blog.csdn.net/xy707707/article/details/80987985)
4. [ffmpeg格式转换基础知识](https://www.cnblogs.com/yongfengnice/p/7133714.html)