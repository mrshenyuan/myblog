---
title: 'phpmyadmin config'
date: 2018-12-06 20:50:47
tags: 
  - phpmyadmin
categories:
  - 零碎笔记
description: 'phpmyadmin config'
---

安装命令

```
cd /var/www/html/
wget https://files.phpmyadmin.net/phpMyAdmin/4.8.3/phpMyAdmin-4.8.3-all-languages.zip
unzip phpMyAdmin-4.8.3-all-languages.zip 
mv phpMyAdmin-4.8.3-all-languages phpMyAdmin
cd phpMyAdmin/
cp config.simple.inc.php config.inc.php
vim config.inc.php
```

config.inc.php配置案例

```
<?php
/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 * phpMyAdmin sample configuration, you can use it as base for
 * manual configuration. For easier setup you can use setup/
 *
 * All directives are explained in documentation in the doc/ folder
 * or at <https://docs.phpmyadmin.net/>.
 *
 * @package PhpMyAdmin
 */
//https://lastpass.com/generatepassword.php
$cfg['blowfish_secret'] = '6&9NlqJNCvdG'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */
$cfg['CheckConfigurationPermissions'] = false;
$i = 0;

$i++;
/* Authentication type */
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['host'] = '127.0.0.1';
$cfg['Servers'][$i]['compress'] = false;
$cfg['Servers'][$i]['AllowNoPassword'] = false;
$cfg['Servers'][$i]['user'] = 'root';
$cfg['Servers'][$i]['password'] = 'root';

$cfg['DefaultLang'] = 'zh_CN';
$cfg['ServerDefault'] = 1;

/**
 * Directories for saving/loading files from server
 */
$cfg['UploadDir'] = '';
$cfg['SaveDir'] = '';


```


1. [解决phpmyadmin配置文件的权限问题](https://blog.csdn.net/Enarm/article/details/57083515)
2. ubuntn update phpmyadmin [How to upgrade PHPmyAdmin [revisited]](https://askubuntu.com/a/1057085)