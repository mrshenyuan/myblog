---
title: 'vscode软件窗口标题栏显示文件完整路径'
date: 2019-01-31 21:09:47
tags: 
  - vscode
categories:
  - 工具
description: 'vscode软件窗口标题栏显示文件完整路径'
---

vscode编辑器窗口标题默认就显示个文件名字，按以下设置即可显示文件完整路径：

* 菜单栏：“文件(File)”→“首选项(Preferences)”→“设置(Settings)”，进入用户配置界面

* 在软件默认的配置界面搜索关键字 “window.title”，将这一行配置复制到右边的用户配置界面中，并将 `activeEditorShort` 修改为 `activeEditorLong`

![](https://raw.githubusercontent.com/wakasann/diarynote/master/draft/uploads/190905/fs20190909000031.jpg)

保存后，再编辑文件时，软件窗口的标题栏上就可以看到当前文件的完整路径了。

摘抄于: [vscode软件窗口标题栏显示文件完整路径](http://www.bmqy.net/1451.html)