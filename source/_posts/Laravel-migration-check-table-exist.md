---
title: Laravel 迁移检查表是否存在
date: 2019-07-09 21:45:34
tags: Laravel
categories:
- Laravel
description: 使用`Schema::hasTable('TableName')`在迁移检查表是否存在
---

```php
Schema::hasTable('TableName'); //检查表释放存在
Schema::hasColumn('tableName', 'columeName'); //检查表是否存在某个字段
```

eg. `alert_test_add_timesteamps_field.php` 文件，添加`updated_at`和`created_at` 2个字段

```php

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertTestAddTimesteampsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('test', function (Blueprint $table) {
            if (!Schema::hasColumn('test', 'updated_at')) {
                $table->timestamp('updated_at')->nullable();
            }

            if (!Schema::hasColumn('test', 'created_at')) {
                $table->timestamp('created_at')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('test', function (Blueprint $table) {
            if (Schema::hasColumn('test', 'updated_at')) {
                $table->dropColumn('updated_at');
            }

            if (Schema::hasColumn('test', 'created_at')) {
                $table->dropColumn('created_at');
            }
        });
    }
}
```


迁移的常用命令(前提:已`cd`到项目的根目录下)

```
php artisan make:migration create_tablename_tables --create=tableName #创建新表时使用
php artisan make:migration alert_tablename_coumn_field --table=tableName #修改表中的字段时使用
php artisan migrate # 执行迁移
php artisan migrate --seed # 执行迁移及生成测试数据
php artisan migrate:rollabck # 回滚上一步迁移
php artisan migrate:refersh # 重新执行迁移
php artisan migrate:refresh --seed # 重新生成表和填充数据
```


##### References
1. [Is there any way to detect if a database table exists with Laravel](https://stackoverflow.com/a/15938132)
