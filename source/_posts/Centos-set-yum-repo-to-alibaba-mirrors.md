---
title: Centos yum的源 设置为阿里云源
date: 2019-07-09 23:06:28
tags: yum源 Linux
categories:
- Linux
description: 记一次设定Centos yum源为阿里云源
---

在 [阿里巴巴镜像站页面](https://opsx.alibaba.com/mirror?lang=zh-CN)，在`centos` 操作的帮助，有介绍 `wget`和`curl` 2种方式来下载`CentOS-Base.repo`

1. 备份
```
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
```

2. 下载新的CentOS-Base.repo 到/etc/yum.repos.d/

2.1.  CentOS-Base.repo 在阿里巴巴镜像站的链接

CentOS 5: `http://mirrors.aliyun.com/repo/Centos-5.repo`
CentOS 6: `http://mirrors.aliyun.com/repo/Centos-6.repo`
CentOS 7: ` http://mirrors.aliyun.com/repo/Centos-7.repo`

使用 wget 软件进行下载

```
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-5.repo #CentOS 5
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo #CentOS 6
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo #CentOS 7
```

或者使用 curl 软件进行下载

```
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-5.repo #CentOS 5
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo #CentOS 6
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo #CentOS 7
```

或者直接下载 对应版本的`CentOS-Base.repo`文件,在本地使用编辑器打开，将本地打开的`CentOS-Base.repo`文件所有内容都复制到 centos的`/etc/yum.repos.d/CentOS-Base.repo`文件中去

3. 运行 `yum makecache` 命令生成缓存

##### References
1. [阿里巴巴镜像站页面](https://opsx.alibaba.com/mirror?lang=zh-CN) 点击列表的`centos`行的操作列，点击"帮助" 就会弹出设置教程咧